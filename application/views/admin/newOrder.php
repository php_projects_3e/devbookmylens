<div class="box">
    <div class="box-body">
      <div class="col-lg-12">
        <form class="form-horizontal" action="insertNewOrderConfirm" method="post">
          <input type="hidden" name="valueJson" value="" id="valueJson">
          <div class="ui-widget">
            <div class="col-lg-2">
              Client Name
            </div>
            <div class="col-lg-4">
              <input require type="text" name ="client" id="clientAutoComplete" class="form-control" />
            </div>
            <div class="col-lg-2">
              Item Name
            </div>
            <div class="col-lg-3">
              <input require type="text" id="itemAutoComplete" class="form-control" />
            </div>
            <div class="col-lg-1">
              <button type="button" onclick="showModal();return false;">Add</button>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-lg-2">
            Pickup Address
          </div>
          <div class="col-lg-4">
            <select class="form-control" id="pickAddr" name="pickAddr"></select>
          </div>
          <div class="col-lg-2">
            Return Address
          </div>
          <div class="col-lg-3">
            <select class="form-control" id="returnAddr" name="returnAddr"></select>
          </div>
          <div class="clearfix"></div>

          <table class="table hide" id="orderTable">
            <thead>
              <th>Item ID</th>
              <th>Item Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Days</th>
              <th>Price</th>
            </thead>
            <tbody>
            </tbody>
          </table>
          <div class="clearfix"></div>
          <br>
          <button id="saveBtn" class="hide" type="submit" onclick="return formSubmit();">Save</button>
        </form>
      </div>
      <div class="clearfix"></div>
    </div>
</div>

<div class="modal fade" id="pickDateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document" style="width:50%;height:600px">
    <div class="modal-content" >
      <div class="modal-header bg-warning" style="border-bottom: 1px solid #fff;background-color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger text-uppercase" id="myModalLabel">Select Order Dates</h4>
      </div>
      <div class="modal-body">
        <iframe id="uiFrame" src="" width="100%" height="600px"></iframe>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">

  clientList = <?php echo json_encode($clientList);?>;
  itemList = <?php echo json_encode($itemList);?>;
  modelUIUrl = "<?php echo admin_url('orders/pickOrderDatesUI/'); ?>";

  function formSubmit()
  {
    var json = '{';
    var otArr = [];
    var tbl2 = $('#orderTable tbody tr').each(function(i) {
      x = $(this).children();
      var itArr = [];
      x.each(function() {
         itArr.push('"' + $(this).text() + '"');
      });
      otArr.push('"' + i + '": [' + itArr.join(',') + ']');
    })
    json += otArr.join(",") + '}';
    $('#valueJson').val(json);
    if($('#valueJson').val() == ''
          || $('#clientAutoComplete').val() == '')
      return false;
    return true;
  }

  window.closeModal = function(){
    $('#pickDateModal').modal('hide');
  };

  window.addProduct = function(id, name, date1, date2, dates, price){
    $('#orderTable')
      .append('<tr><td>'+id+'</td><td>'+name+'</td><td>'+date1+'</td><td>'+date2+'</td><td>'+dates+'</td><td>'+price+'</td><td ><button type="button" onclick="removeRow(this);">Remove</button></td></tr>');
    $('#itemAutoComplete').val('');

    if($('#orderTable tr').length > 0)
    {
      $('#orderTable').removeClass('hide');
      $('#saveBtn').removeClass('hide');
    }
    else
    {
      $('#orderTable').addClass('hide');
      $('#saveBtn').addClass('hide');
    }
  }

  function removeRow(ele)
  {
    ele.closest('tr').remove();

    if($('#orderTable tr').length > 0)
    {
      $('#orderTable').removeClass('hide');
      $('#saveBtn').removeClass('hide');
    }
    else
    {
      $('#orderTable').addClass('hide');
      $('#saveBtn').addClass('hide');
    }
  }
  function showModal()
  {
    $('#pickDateModal').modal('show');
    $('#uiFrame').attr('src', modelUIUrl+ ($('#itemAutoComplete').val()));
  }

  $(function(){
    $( "#clientAutoComplete" ).autocomplete(
    {
      source:custom_source_client,
      select: function (event, ui) {
          pickAddr = $('#pickAddr');
          returnAddr = $('#returnAddr');
          pickAddr.html('<option value="1">Store Pickup</option>');
          returnAddr.html('<option value="1">Store Pickup</option>');
          $.getJSON("getAddress/"+ui.item.id, function(result){
              $.each(result, function(i, field){
                str = field.addressType +" | "+field.addressLine1+", "+field.addressLine2+", "+field.addressCity+", "+field.addressState+", "+field.addressPin+", "+field.addressLandmark
                pickAddr.append('<option value="'+field.addressID+'">'+str+'</option>');
                returnAddr.append('<option value="'+field.addressID+'">'+str+'</option>');
              });
          });
          event.preventDefault();
          this.value = ui.item.number+' | '+ui.item.email+' | '+ui.item.label;
      }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li class='ui-menu-item'></li>" )
            .append( item.number+' | '+item.email+' | '+item.label)
            .appendTo( ul );
    };

    $( "#itemAutoComplete" ).autocomplete(
    {
      source:itemList
    });

  })

  function custom_source_client(request, response) {
      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(clientList, function(value) {
          return matcher.test(value.label)
                  || matcher.test(value.number)
                    || matcher.test(value.email);
      }));
  }

</script>
