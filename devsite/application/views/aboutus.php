<div class="clearfix"></div>
<br>

	
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
			<h3 class="font-family-helvetica font-weight-bold color-c1272c">About Us</h3>
<div id="content" class="font-family-helvetica text-justify">

<p> The short version is this:<b> Rent . Click . Cherish</b></br></p>
<p>A Photography Equipment Rental Company.</p>
<p>Our mission is to provide the best of photographic and cinematic equipments to our customers by delivering superior , cutting-edge gear with exceptional customers service.  </p>
<p>You choose what you want, when you want and how long you want to rent it for.  We ship gear directly to you.  Our entire rental process is done completely through our website but if you have a request you can call us and talk to one of our person working at our main office in Bangalore.</p>
<p><b>Our Beginning.</b></p>
<p>Starting with just a handful of lenses in 2011, which was shared only with known friends at an affordable cost turned into a full time rental business which started to cater to a larger audience in 2012.  We have made every effort to hire good , local talents who are focused to provide great customer service and who are deeply passionate about photography and videography.</p>
<p>Over the last few years, customers have entrusted the BML team to provide an ever growing selection of cameras, lenses, lighting kits, audio equipment, and production support systems that are suitable for both novices and pros.</p>
<p>All our customer are the “Silent Partners” of bookmylens.com and they are responsible for our success now and in future . We have been entrusted by our customers to take us along with them on their photographic and cinematic journey. </p>
</br>
		</div>
		
	     <div class="col-sm-2 col-md-2"></div>
