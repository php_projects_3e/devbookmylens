<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductListByCategoryName($categoryNames,$offset=0, $perPage=-1,$order='itemName', $orderType = 'asc') {
      
	  if($categoryNames == "Tripods, Rigs & Accessories")
	  {
		$categoryNames = array("'".$categoryNames."'");
	  }
	  else
	  {
		$arr = explode(',', $categoryNames);
	  $categoryNames = array();
		foreach($arr as $row)
		  {
			$categoryNames[] = "'".trim($row)."'";
		  }
	  }
	  $sql="SELECT a.itemId, a.`itemDescription`, a.`itemImage1` as 'itemImage', a.`itemName`, a.`itemSampleImagePath`, a.seasonID
            FROM `itemmaster` a
            WHERE a.`itemStatus` = 1
            and a.`itemCategory` in (
                SELECT `categoryID` FROM `category_tb` WHERE `category_Name` in (".implode(', ', $categoryNames).")
          ) order by $order $orderType ";
          /* 'SELECT a.itemId, a.`itemDescription`, a.`itemImage1` as 'itemImage', a.`itemName`, a.`itemSampleImagePath`, a.seasonID
            FROM `itemmaster` a
            WHERE a.`itemStatus` = 1
            and a.`itemCategory` in (
                SELECT `categoryID` FROM `category_tb` WHERE `category_Name` in ('DSLR')
          ) order by itemName asc '
         */
      if($perPage > 1 ) $sql +="LIMIT $offset,$perPage";
      
	  return $this->bml_database->getResults($sql);
    }

    public function getProductCountByCategoryName($categoryNames) {
      $sql="SELECT count(*) as ProductCount
            FROM `itemmaster` a
            WHERE a.`itemStatus` = 1
            and a.`itemCategory` in (
                SELECT `categoryID` FROM `category_tb` WHERE `category_Name` in ('$categoryNames')
          )";

      $result = $this->bml_database->getResults($sql);
      return (array_key_exists(0, $result)?(array_key_exists(0, $result[0])?(array_key_exists('ProductCount', $result[0][0]) ? $result[0][0]['ProductCount']: 0):0):0);
    }

    public function getFeaturedProductList() {
      $sql="SELECT a.itemId, a.`itemDescription`, a.`itemImage1` as 'itemImage', a.`itemName`, a.`itemSampleImagePath`, a.seasonID
            FROM `itemmaster` a, featured_equipments b
            WHERE a.itemId = b.itemId
            and a.`itemStatus` = 1";
      return $this->bml_database->getResults($sql);
    }

    public function getProductListByCategoryAndBrand($categoryName, $brands) {
      $sql="SELECT a.itemId, a.`itemDescription`, a.`itemImage1` as 'itemImage', a.`itemName`, a.`itemSampleImagePath`, a.seasonID
            FROM `itemmaster` a
            WHERE a.`itemStatus` = 1
            and a.`itemBrand` = (SELECT `itemBrandID` FROM `tbl_item_brand` WHERE `itemBrandName` = '$brands')
            and a.`itemCategory` in (
                SELECT `categoryID` FROM `category_tb` WHERE `category_Name` in ('$categoryName')
          )";
      return $this->bml_database->getResults($sql);
    }

    public function getPriceByProductIDAndSeasonID($productID, $seasonID)
    {
      $sql = "SELECT `days`, `price` FROM `pricemaster` WHERE `itemId` = $productID and `seasonId` = $seasonID";
      return $this->bml_database->getResults($sql);
    }

    public function getProductDetails($productName)
    {
      $sql = "SELECT a.`itemId`, b.itemBrandName, c.category_Name, a.`itemDescription`, a.`itemImage2` as 'itemImage', a.`itemName`, a.`seasonID`, a.`itemSpecification`
            FROM `itemmaster` a
            JOIN tbl_item_brand b
            on a.`itemBrand` = b.itemBrandID
            JOIN category_tb c
            ON a.`itemCategory` = c.categoryID
            WHERE a.`itemName` = '$productName'
            and a.`itemStatus` = 1";

      return $this->bml_database->getResults($sql);
    }

}



?>
