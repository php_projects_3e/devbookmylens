<?php
  $date1 = '';
  $date2 = '';
  $days = '';
  $rowid = '';
  $price = '';
  $waiting = false;
  if(!empty($productInCart))
  {
    $date1 = $productInCart['options']['startDate'];
    $date2 = $productInCart['options']['endDate'];
    $waiting = $productInCart['options']['waitingList'];
    $rowid = $productInCart['rowid'];
    $price = $productInCart['subtotal'];
  }
?>


<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8">
  <div class="row">
    <div class="col-md-12">
    <?php
      $breadcrumbText = '';
      foreach ($breadcrumb as $breadcrumbRow){
        $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
        $breadcrumbText .= " > ";
      }
      echo rtrim($breadcrumbText, " > ");
    ?>
    </div>
  </div>
  <div class = "row">
    <div class="col-md-4 padding-0px margin-top-10px">
      <div class="col-md-12 border-5px-solid-C1272D height-295px">
        <div >
          <img src="<?php echo site_url('images/viewImages/'.$productDetails['itemImage']);?>" alt="Images" class="width-85">
        </div>

          <h3><center><b><?php echo $productDetails['itemName'];?></b></center></h3>
          <div class="clearfix"></div>
          <br>

      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 border-5px-solid-C1272D padding-0px" style="height: 320px !important">
        <div class="full-width-tabs">
          <ul class="tabs nav  nav-tabs margin-bottom-15px background-c1272d width-1005 border-bottom-0px">
            <li class="take-all-space-you-can width-33" ><a class="text-align-center padding-8px-0px"  href="#overview"
              data-toggle="tab">Overview</a></li>
            <li class="active take-all-space-you-can width-33" ><a class="text-align-center padding-8px-0px" href="#pricelist"
              data-toggle="tab">Price</a></li>
            <li class="take-all-space-you-can width-33" ><a class="text-align-center padding-8px-0px" href="#specification"
              data-toggle="tab" >Tech Spec</a>
            </li>
          </ul>

          <div id="myTabContent" class="tab-content padding-10px">
              <div class="tab-pane fade " id="overview">
                  <p class="text-justify" style="height: 200px;overflow: auto;"><?php echo $productDetails['itemDescription']; ?></p>
              </div>
              <div class="tab-pane fade active in" id="pricelist">
                  <p>
                    <table class="table table-striped table-bordered table-condensed">
                      <tr>
                        <th>No. of Days</th>
                        <th>Per Day Rates</th>
                      </tr>
                      <?php
                          $priceListArr = [];
                          foreach ($productDetails['priceList'] as $priceList) {
                            $priceListArr[$priceList['days']] = $priceList['price'];
                      ?>
                        <tr>
                          <td> <?php echo $priceList['days']."+";?></td>
                          <td>
                            <?php
                              if($priceList['price']) {
                                echo $priceList['price']."&nbsp;INR";
                              }
                              else {
                                echo "* Contact Us";
                              }
                            ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </table>
                  </p>
              </div>

			  <div class="tab-pane fade" id="specification">
				<p class="text-align: justify;" style="height: 200px;overflow: auto;"><?php echo $productDetails['itemSpecification']; ?></p>
              </div>
          </div>
        </div>
      </div>
    </div>
	<div class="col-md-8 border-5px-solid-C1272D margin-top-10px" style="height: 615px !important;">
		<?php if($isLoggedIn && $isApproved) { ?>
    
		<p>
            Select Rent Dates:
          </p>
          <div class="datepicker"></div>
          <div class="clearfix"></div>
          <table class="legend">
			<tbody>
				<tr>
					<td class = "unavailable" style = "width: 20px;height: 20px;border: 1px solid;"></td>
					<td>Unavailable</td>
					<td class = "waiting" style = "width: 20px;height: 20px;border: 1px solid;"></td>
					<td>Waiting</td>
					<td class = "closed" style = "width: 20px;height: 20px;border: 1px solid;"></td>
					<td>Holiday</td>
					<td class = "" style = "width: 20px;height: 20px;border: 1px solid;"></td>
					<td>Available</td>
					<td class = "dp-highlight" style = "width: 20px;height: 20px;border: 1px solid;"></td>
					<td>Selected</td>
				</tr>
			</tbody>
		  </table>
		  <div class="clearfix"></div>
          <br>
		  <table class="table table-striped table-bordered table-condensed">
            <tr><th>From:</th><td><label id="ldate1"><?php echo $date1;?></label></td></tr>
            <tr><th>To:</th><td><label id="ldate2"><?php echo $date2;?></label></td></tr>
            <tr><th>Number of days:</th><td><label id="ldays"><?php echo $days;?></label></td></tr>
            <tr><th>Rental Prices</th><td><label id="lprice"><?php echo $price;?></label></td></tr>
          </table>
		<?php } else {?>
			<div style="height:200px"></div>
		<?php } ?>
            <form id="hiddenForm" name="myForm" method="post" action="<?php echo site_url('cart/view/');?>" onsubmit="return validateForm()">
              <input type="hidden" name="orderID" value="<?php echo $rowid;?>">
              <input type="hidden" id="date1"  name="date1" value="<?php echo $date1;?>">
              <input type="hidden" id="date2" name="date2" value="<?php echo $date2;?>">
              <input type="hidden" id="days" name="days" value="<?php echo $days;?>">
              <input type="hidden" id="price" name="price" value="<?php echo $price;?>">
              <input type="hidden" name="productName" value="<?php echo $productDetails['itemName'];?>">
              <input type="hidden" name="productID" value="<?php echo $productDetails['itemId'];?>">
              <?php if($isLoggedIn && $isApproved) { ?>
                <div class="display-table bottom-13px right-0 left-0 height-40px width-100 text-align-center">
                  <div id="contact_submit" class="display-table-cell vertical-align-middle margin-top-5px">
                    <input type="submit"  class="button font-weight-bold font-size-18px color-EAD608 margin-left-0px  height-40px padding-top-8px" style="background: #C1272D;" value="Rent Now"  onclick="return checkWaiting();">
                  </div>
                </div>
                <h5 class="error">This field is required</h5>
              <?php } else if($isLoggedIn){?>
                  <div class="display-table bottom-13px right-0 left-0 height-40px width-100 text-align-center">
                    <a href="<?php echo site_url('myaccount');?>" data-toggle="modal" class="text-center">
                      <div class="button font-weight-bold font-size-18px color-EAD608 margin-left-0px  height-40px padding-top-8px" style="background: #C1272D;" >
                        Account Info
                      </div>
                    </a>
                    <br><span class="text-danger">Account Unapproved</span>
                  </div>
              <?php } else {?>
                  <div class="display-table bottom-13px right-0 left-0 height-40px width-100 text-align-center">
                    <a href="#signupModal" data-toggle="modal" class="text-center">
                      <div class="button font-weight-bold font-size-18px color-EAD608 margin-left-0px  height-40px padding-top-8px" style="background: #C1272D;">
                        Login To Rent
                      </div>
                    </a>
                  </div>
              <?php } ?>
            </form>
    </div>
  </div>
</div>
<div class="col-sm-2 col-md-2"></div>

<!-- Modal for providing waiting list -->
<div class="modal fade" id="waitingList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog width-350px" role="document">
    <div class="modal-content" >
      <div class="modal-header bg-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger text-uppercase" id="myModalLabel">Product Not Available</h4>
      </div>
      <div class="modal-body">
       <p class="text-justify"><b>Product is not available for all or some of the selected dates.</b></p>
        <p class="text-center text-danger"><b>Do you want to add product to waiting list ?</b></p>
        <br>


        <div class="text-center">
          <button class="btn btn-success btn-sm" onclick="$('#hiddenForm').submit();">YES</button>&nbsp; &nbsp;
          <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close">NO</button>
        </div>

        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<!-- Modal for please select data list -->
<div class="modal fade" id="selectDateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog width-300px" role="document" >
    <div class="modal-content" >
      <div class="modal-header bg-warning border-bottom-1px-solid-fff background-color-#fff">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger text-uppercase" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
       
        <p class="text-center text-danger"><b>Please Select Date.</b></p>
        <br>


        <div class="text-center">
          
          <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close">OK</button>
        </div>

        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<div id="dialog-confirm" class="hide">
  <p class="text-justify">Product is not available for all/some of the selected dates.</p>
  <p class="text-center text-danger">Do you want to add product to waiting list ?</p>
</div>
<script type="text/javascript">
  var priceArray = <?=json_encode($priceListArr)?>;
  var unAvailableDates = <?=json_encode($unAvailableDates);?>;
  var holidayList = <?= json_encode($holidayList);?>;
  var waitingList = false;

  function validateForm() {
    var x = document.forms["myForm"]["date1"].value;
    if (x == null || x == "") {
        
    $('#selectDateModal').modal('show');
    return false;
  }
  return true;
  }

  //override the addClass function
  var originalAddClassMethod = jQuery.fn.addClass;
  jQuery.fn.addClass = function(){
      // Execute the original method.
      var result = originalAddClassMethod.apply( this, arguments );
      addTitleTextRentCal();
      return result;
  }

</script>

