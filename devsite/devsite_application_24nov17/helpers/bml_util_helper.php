<?php
function generateAdminClientURL($path)
{
  $path = ($path == "")? site_url('assets/dist/img/avatar.png') : site_url('uploads/'.$path);
  return "<img style ='width: 15%;' src='$path'/>";
}

function generateAdminFileDownload($path)
{
  return ($path != '') ? "<a target = '_blank' href='".site_url("uploads/".$path)."'>".get_file_extension($path)."</a>": '';
}

function generateFileDownload($path)
{
  $pos = strrpos($path, "/");
  return ($path != '') ? "<a target = '_blank' href='".site_url("uploads/".$path)."'>".substr($path, $pos+1)."</a>": '';
}

function get_file_extension($file_name) {
  return substr(strrchr($file_name,'.'),1);
}

function formateDateString($date)
{
  return (mdate('%D, %d %M %Y %h:%i %A %T',strtotime($date)));
}

function dateFromMysqlDate($date)
{
  return mdate('%d-%M-%Y',strtotime($date));
}

function dateFormat($date, $format='%d-%M-%Y')
{
  return mdate($format,strtotime($date));
}

function dateTimeFromMysqlDate($date)
{
  return mdate('%D, %d %M %Y %h:%i %A %T', strtotime($date));
}

function qgFormateDate()
{
  return mdate('%D, %d %M %Y %h:%i %A %T', now());
}

function currency_format($number, $decimalPoints = 2)
{
  setlocale(LC_ALL, 'en_IN');
  if(function_exists('money_format'))
    return money_format("%n",$number);
  else
    return '₹ '.(number_format($number, $decimalPoints));
}

function my_number_format($number, $decimalPoints = 2)
{
  return (number_format($number, $decimalPoints));
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

?>
