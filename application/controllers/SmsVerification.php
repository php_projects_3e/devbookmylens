<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class smsVerification extends CI_Controller{


public function index()

{

        
        if(!$this->user_session->isLoggedIn())

        {

                if($this->user_session->getSessionVar('email'))

                {   $otp=$this->user_session->getSessionVar('otp');

                //var_dump($otp);
                        
                $data['category'] = $this->bml_read_json->readRentMenu();
        
                
        
                $this->load->model('commonModel');
        
                $resultSet = $this->commonModel->getMainPageCategories();
        
                // Getting location name
        
                $locationSet=$this->commonModel->getAllLocationNames();
        
                $data['locationName']=(array_key_exists(0, $locationSet)) ? $locationSet[0]: array();
        
        
        
                $data['mainPageCategory'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
        
        
        
                $result = $this->commonModel->getLatestDetails();
        
                $itemResult=$this->commonModel->getProductDetailsBySatusActive();
        
                $result = (array_key_exists(0, $result))? $result[0]: array();
        
                
        
                $data['tableRows'] = $result;
        
                $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
        
                $data['itemResult'] = $itemResult;
        
                
        
                $homeSlide = $this->commonModel->getHomeSliderDetails();
        
                
        
                $homeSlide = (array_key_exists(0, $homeSlide))? $homeSlide[0]: array();
        
                
        
                $data['homeSlide'] = $homeSlide;
        

                        $this->load->helper(array('form', 'url'));

                        $this->load->view('head');

                        $this->load->view('header',$data);

                        $this->load->view('smsVerification');

                        $this->load->view('footer');

                        $this->load->view('footerDetails');

                }else{

                        redirect(site_url(''));

                }

        }else{

                redirect(site_url(''));

        }

}



public function valid_user_session($value)

{       if ($value === null)

        {

                $this->form_validation->set_message('valid_user_session','Invalid user session');

                return false;                     

        }

        else

        {

                //$this->form_validation->set_message('user_session','Invalid user session');

                return TRUE;

        }

}

public function valid_otp($value)

{

        $emailId=$this->user_session->getSessionVar('email');

        $otp=$this->user_session->getSessionVar('otp');

        $this->load->model('myaccountModel');

        //$otpResult = $this->myaccountModel->getUserOTP($emailId);

        //$otpNumber=$otpResult['otpNumber'];

        $value=(int)$value;

        if ($value !== $otp)

        {       $this->form_validation->set_message('valid_otp','Invalid OTP');

                return FALSE;

        }

        else

        {

                return true;

        }

} 

public function check(){

    $json=[];

    $json = array("status"=>"failure","errorcode" =>"MISMATCH","message"=>"required fields must be filled");

    $entered_otp_number=$this->input->post('otp');

    // Email from session

    $user_email=$this->input->post('email');

    

    // Loading MyaccountModel

    $this->load->model('myaccountModel');



    // Get User OTP from myaccountModel against email

    $otpResult = $this->myaccountModel->getUserOTP($user_email);

   

    // OTP from Resulted query

    $otpNumber=$otpResult['otpNumber'];

    //var_dump("your otp=".$this);

    var_dump("entered OTP=".$entered_otp_number);

    if($entered_otp_number==" "){

        $json = array("status"=>"failure","errorcode" => "EXISTS","message"=>"E-mail already exists. Please Login.");

    }elseif($entered_otp_number!== $otpNumber){

        $json = array("status"=>"failure","errorcode" => "EXISTS","message"=>"E-mail already exists. Please Login.");

    }else{

        $json=array("status"=>"success");

    }

    echo json_encode($json);

}

public function checkOptionsSelected(){ 

    
    /* 

    * Loading form validation library.

    */

    

    $this->load->library('form_validation');

    $this->form_validation->set_rules('otp','OTP','required|numeric|trim|exact_length[6]|callback_valid_otp');

    $this->form_validation->set_rules('email','E-mail','callback_valid_user_session');

    $this->form_validation->set_error_delimiters("<p class='text-danger'>","<p>");

    // validation

    if($this->form_validation->run()){         

         // Entered Details

    //$entered_otp_number=$this->input->post('otp');

    //$entered_otp_number=(int)$entered_otp_number;

    

    // OTP number   

    //$entered_otp_number=trim($entered_otp_number," ");

    // Email from session

    $this->load->model('myaccountModel');

    //$resultSet=$this->myaccountModel->getUserDetails($user_email);                

    /*$fname=$resultSet['firstname'];

    $lname=$resultSet['lastname'];

    $companyPerson=$resultSet['companyPerson'];

    $phone=$resultSet['mobileNumber'];

    $pwd1=$resultSet['password'];

    $emailId=$resultSet['emailId'];

    $gender=$resultSet['gender'];

    $locationKey=$resultSet['locationKey'];

    $otpNumber=$resultSet['otpNumber'];

    $OTP_length=strlen($entered_otp_number)===6?true:false;

    $validOTP=(!empty($entered_otp_number) && is_numeric($entered_otp_number) &&$OTP_length );

    var_dump("Is valid=".$validOTP);

    if($validOTP){*/

    // Loading MyaccountModel

    //$this->load->model('myaccountModel');

    // Get User OTP from myaccountModel against email

   // $otpResult = $this->myaccountModel->getUserOTP($user_email);

   

    // OTP from Resulted query

    //$otpNumber=$otpResult['otpNumber'];  

   // Verify OTP with entered OTP

   // if($otp===$entered_otp_number){

        //Getting Email from session

    $fname=$this->input->post('fname');

    //$this->user_session->getSessionVar('fname');

    $lname=$this->input->post('lname');

    //$this->user_session->getSessionVar('lname');

    $gender=$this->input->post('gender');

    //$this->user_session->getSessionVar('gender');

    $emailId=$this->input->post('email');

    //$this->user_session->getSessionVar('email');

    $pwd1=$this->input->post('pwd1');

    //$this->user_session->getSessionVar('pwd1');

    $companyPerson=$this->input->post('companyPerson');

    //$this->user_session->getSessionVar('companyPerson');

    $phone=$this->input->post('phone');

    //$this->user_session->getSessionVar('phone');

    $locationKey=$this->input->post('locationKey');

    //$this->user_session->getSessionVar('locationKey');

    $otp=$this->user_session->getSessionVar('otp'); 

    // Insert into customer table

    //$result=$this->myaccountModel->tempUser($companyPerson,$fname,$lname,$gender,$emailId,$pwd1,$phone,$otp,$locationKey);

    //$result=$this->myaccountModel->registerUser($companyPerson,$fname,$lname,$gender,$emailId,$pwd1,$phone,$locationKey);
    $result=$this->myaccountModel->registration($companyPerson,$fname,$lname,$gender,$emailId,$pwd1,$phone,$locationKey);
    
    $code = $result[0][0]['varReferralCode'];

    $customerID = $result[0][0]['customerID'];

    $customerNumber = $result[0][0]['customerNumber'];



    $data['email'] = $emailId;

    $data['firstName'] = $fname;

    $data['code'] = $code;

    $data['customerID'] = $customerID;

    $data['customerNumber'] = $customerNumber;

    $data['locationKey']= $locationKey;

     //echo json_encode($result);

    // Loading Email Library  

    //$this->load->library("send_email");

   // $this->send_email->sendWelcomeEmail($data);    

    $this->loginWithOtp($emailId,$pwd1); 

   // }else{    

   // $this->form_validation->set_message('valid_otp', 'Invalid OTP');

    //$json= array("status"=>"failure","errorcode" => "MISMATCH","message"=>"Invalid OTP number");

    //echo json_encode($json);

     /*$this->load->view('head');

    $this->load->view('header');

    $this->load->view("smsVerification");

    $this->load->view('footer');

    $this->load->view('footerDetails');*/

    //}

    }else{

    //$json= array("status"=>"failure","errorcode" => "MISMATCH","message"=>"Please enter Valid OTP number");

    $this->load->view('head');

    $this->load->view('header');

    $this->load->view("smsVerification");

    $this->load->view('footer');

    $this->load->view('footerDetails');

    //redirect(site_url('smsVerification'));

    }

    //echo json_encode($json);

}

public function loginWithOtp($email,$password){

   // var_dump("inside login with otp");

        $this->load->model('myaccountModel');

        if($status = $this->myaccountModel->validateLogin($email, $password))

        {

        $this->user_session->login($email);

        //$json = $arrayName = array('status' => 'success', "isAdmin" => $this->user_session->getSessionVar('is_admin_login'));

        //$json = $arrayName = array('status' => 'success');

        redirect(site_url('myaccount'));

        }

        else

        {

        //$json = $arrayName = array('status' => 'failure');

        redirect(site_url(''));

        }  

        //echo json_encode($json);

}



public function generateOTPNumber()

{      

    //var_dump("generateOTPNumber");

        //$this->load->helper(array('form', 'url'));

        // Loding Form Validation library.

        $this->load->library('form_validation');

        // Verifing wheather E-mail is present in Session.

        //$this->form_validation->set_rules('email','E-mail','callback_valid_user_session');

        // If valid session 

        $emailId=$this->user_session->getSessionVar('email');

        //if($this->form_validation->run()){

        //{      var_dump($this->fname);

                // Getting E-mail from session.

                if(!empty($emailId)){

                //var_dump($emailId);

                // Generating random six digit number.

                $otp_number = mt_rand(100000, 999999);

                //var_dump( $otp_number );

                // Loading MyaccountModel file from Models folder.

                $this->load->model('myaccountModel');

                

                /**

                * Updating Otp number in tmp_customer table

                * With respect to email.

                */

                $this->myaccountModel->updateOTP($emailId,$otp_number);

                // Get details of user with email and otp number.

                $resultSet=$this->myaccountModel->getUserDetails($emailId); 

                // phone number from database.

                $phone=$resultSet['mobileNumber']; 

                //var_dump($phone);

                // Loading message library.

                $this->load->library("send_sms");

                // Sending Otp to user.

                $this->send_sms->sendOTP($phone,$otp_number);

                // Redirect to smsVerification page

               // redirect(site_url('smsVerification'));

        }else{  

                // If no session is present then redirect to homepage.

                redirect(site_url(''));

        } 

}


}