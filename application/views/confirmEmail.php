<?php if($email != '' && $status == 'SUCCESS') { ?>

<h3>Welcome back <b><?php echo $email; ?></b>, your email has been confirmed.
Please click here to <a href="#signupModal" class="dropdown-toggle" data-toggle="modal">Log In</a>"</h3>
<?php } else { ?>
<h3>Invalid/expired link. You will be redirected to homepage in 5 seconds</h3>
<script>
  $(function() {
  setTimeout(function(){
    window.location = '<?php echo site_url();?>'; },
  5000);
  });
</script>
<?php } ?>

