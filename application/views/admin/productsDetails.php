          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('products/updateProductDetails') ;?>" method="post">
                   <div class="col-lg-3 col-sm-3">
                    <div class="max-height-200px border-2px-solid-black">
                    <div id="effect-2" class="effects clearfix">
                      <div class="img">
						  <img src="<?php echo site_url('images/viewImages/'.$tableRows[0]['itemImage1']);?>" class="img-responsive">
                          <div class="overlay">
                              <a href="#" class="expand" data-toggle="modal" data-target="#itemImageModal1"><i class="fa fa-cloud-upload"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
					<div class="max-height-200px border-2px-solid-black">
                    <div id="effect-2" class="effects clearfix">
                      <div class="img">
                          <img src="<?php echo site_url('images/viewImages/'.$tableRows[0]['itemImage2']);?>" class="img-responsive">
                          <div class="overlay">
                              <a href="#" class="expand" data-toggle="modal" data-target="#itemImageModal2"><i class="fa fa-cloud-upload"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>
                   </div>
                  <div class="col-lg-5 col-sm-5">
                        <div class="col-lg-4 col-sm-4 text-right">
                          <b>Item ID:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8">
                          <?php $id=$tableRows[0]['itemId'];
                           echo $tableRows[0]['itemId'];?>
                          <input type="hidden" name='itemId' value='<?php echo $tableRows[0]['itemId'];?>'>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Name :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='text' name='itemname' value="<?php echo $tableRows[0]['itemName']; ?>" class="form-control">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1"><b>Item Type :</b></div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='itemType'>
                          <?php foreach ($itemType as $item) { ?>
                            <?php if($item['itemTypeName']== $tableRows[0]['itemTypeName']){ ?>
                                <option value="<?php echo $item['itemTypeID']; ?>" selected>
                                    <?php echo $item['itemTypeName']; ?>
                                </option>
                                <?php } else { ?>
                                <option value="<?php echo $item['itemTypeID']; ?>">
                                  <?php echo $item['itemTypeName']; ?>
                                </option>
                                <?php } ?>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Category Name :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='category'>
                          <?php foreach ($categoryName as $category) { ?>
                            <?php if($category['category_Name']== $tableRows[0]['category_Name']){ ?>
                                <option value="<?php echo $category['categoryID']; ?>" selected>
                                    <?php echo $category['category_Name']; ?>
                                </option>
                                <?php } else { ?>
                                <option value="<?php echo $category['categoryID']; ?>">
                                  <?php echo $category['category_Name']; ?>
                                </option>
                                <?php } ?>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Brand Name :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='itemBrand'>
                          <?php foreach ($brandName as $brand) { ?>
                            <?php if($brand['itemBrandName']== $tableRows[0]['itemBrandName']){ ?>
                                <option value="<?php echo $brand['itemBrandID']; ?>" selected>
                                    <?php echo $brand['itemBrandName']; ?>
                                </option>
                                <?php } else { ?>
                                <option value="<?php echo $brand['itemBrandID']; ?>">
                                  <?php echo $brand['itemBrandName']; ?>
                                </option>
                                <?php } ?>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Number of Stocks in Bengaluru :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='text' name='blrstocks' value="<?php echo $tableRows[0]['Stock']; ?>" class="form-control">
                          <input type='hidden' name='<?php echo $tableRows[0]['locationKey']; ?>' value="<?php echo $tableRows[0]['locationKey']; ?>" class="form-control">

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Number of Stocks in Mysuru:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">

  
                          <input type='text' name='mysrstocks' value="<?php echo $tableRows[1]['Stock']; ?>" class="form-control">
                         
                          <input type='hidden' name='<?php echo $tableRows[1]['locationKey']; ?>' value="<?php echo $tableRows[1]['locationKey']; ?>" class="form-control">
                        
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Status :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select class="form-control" name='itemstatus'>
                            <?php if($tableRows[0]['itemStatus']==1){?>
                            <option value='1' selected>Active</option>
                            <option value='0'>InActive</option>
                            <?php } else {?>
                            <option value='1' >Active</option>
                            <option value='0' selected>InActive</option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Item Desciption :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <textarea type='text' name='itemDescription' rows='5' class="form-control"><?php echo $tableRows[0]['itemDescription']; ?></textarea>
                        </div>
                        <div class="clearfix"></div>
                  </div>

                   <div class="col-lg-4 col-sm-4">
                    <h4 class="text-center">Price Details<span class="btn btn-primary btn-xs add-box float-right padding-left-15px"><i class="fa fa-plus-square-o"></i> ADD PRICE</span></h4>
                    <div class="col-lg-6 col-sm-6 text-right"><b>#Days</b></div>
                    <div class="col-lg-6 col-sm-6"><b>Price</b></div>
                    <div class="clearfix"></div>
                    <div class='addinput'>
                      <?php foreach ($priceResult as $price) { ?>
                      <div class="col-lg-6 col-sm-6 text-right padding-top1">
                       <input type='text' name="days[<?php echo $price['priceId']; ?>]" value="<?php echo $price['days']; ?>" maxlength="4" size="4">
                      </div>
                      <div class="col-lg-6 col-sm-6 padding-top1">
                        <input type='text' name="price[<?php echo $price['priceId']; ?>]" value="<?php echo $price['price']; ?>">
                      </div>
                      <div class="clearfix"></div>
                      <?php } ?>
                    </div>
					
                    <h4 class="text-center">Item Spacification</h4>
                      <textarea type='text' name='itemSpecifi' rows='5' class="form-control"><?php echo $tableRows[0]['itemSpecification']; ?></textarea>

                   </div>
                   <div class="clearfix"></div>
                   <hr>
                   <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <a href="<?php echo admin_url('products/getProductDetails'); ?>" class="btn btn-warning" >CANCEL</a>  <a href="<?php echo admin_url('products/deleteProductByID/'.$id); ?>" onclick='return deleletconfig();' class="btn btn-danger" >DELETE</a></h2>
                   </form>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->

      <!-- Modal for add item type -->
<div class="modal fade" id="itemImageModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">UPLOAD ITEM IMAGE</h4>
      </div>
      <div class="modal-body">
        <form  action="<?php echo admin_url('products/updateItemImage') ;?>" method="post" enctype="multipart/form-data">
          <div class="col-lg-6">
            <input type="file" name="itemimage1"  class="form-control" required>
          </div>
            <input type="hidden" name='itemId' value='<?php echo $tableRows[0]['itemId'];?>'>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>
<div class="modal fade" id="itemImageModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">UPLOAD ITEM IMAGE</h4>
      </div>
      <div class="modal-body">
        <form  action="<?php echo admin_url('products/updateItemImage') ;?>" method="post" enctype="multipart/form-data">
          <div class="col-lg-6">
            <input type="file" name="itemimage2"  class="form-control" required>
          </div>
            <input type="hidden" name='itemId' value='<?php echo $tableRows[0]['itemId'];?>'>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

      <!-- Add input box -->
      <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.add-box').click(function(){
                var n = $('.addinput').length + 1;
                if( 10 < n ) {
                    alert('Stop it!');
                    return false;
                }
                var box_html = $('<div class="addinput"><div class="col-lg-6 col-sm-6 text-right padding-top1"><input type="text" name="days[new_' + n + ']" value="" maxlength="4" size="4"></div><div class="col-lg-6 col-sm-6 padding-top1"><input type="text" name="price[new_' + n + ']" value=""></div></div>');
                box_html.hide();
                $('div.addinput:last').after(box_html);
                box_html.fadeIn('slow');
                return false;
            });

        });
      </script>
      <!-- Delete  confirmation dilouge -->
      <script>
      function deleletconfig(){

      var del=confirm("Are you sure you want to delete this record?");
      if (del==true){
         alert ("record deleted")
      }
      return del;
      }
      </script>
      

