

<div class="clearfix"></div>

<br>

<div class="col-sm-2 col-md-2"></div>

  <div class="col-sm-8 col-md-8">

  <div class="row">

    <div class="col-md-12">

    <?php

      $breadcrumbText = '';

      foreach ($breadcrumb as $breadcrumbRow){

        $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];

        $breadcrumbText .= " > ";

      }

      echo rtrim($breadcrumbText, " > ");

    ?>

    </div>

  </div>

    <div class="col-sm-12 col-md-12">

      <div class="col-sm-8 col-md-8">

        <h2>Shopping Cart</h2>

        <?php if( isset($newProduct)) { ?>

			<?php if(isset($cannotReturn) && isset($cannotReturn) && !($cannotReturn || $cannotCollect)) { ?>

				<p class="font-size-13px color-5CB85C font-family-helvetica padding-10px-0px"><b><i class="fa fa-check-square"></i> 

				<?php echo $newProduct."</b> has been added to cart successfully.</p>";

			}else if(isset($cannotReturn) && isset($cannotReturn) && $cannotReturn || $cannotCollect) {?>

				<p class="font-size-13px text-danger font-family-helvetica padding-10px-0px">

					<b><i class="fa fa-close"></i> <?=$newProduct?></b> cannot be added to cart as we are on holiday on these days.

				</p>	

			<?php }?>

		<?php }?>

      </div>

      <div class="col-sm-4 col-md-4">

        <a href="<?php echo site_url('cart/checkout/'); ?>">

          <button  class=" btn btn-success float-right">

            Proceed to Checkout

          </button>

        </a>

      </div>



    </div>

    <div class="col-sm-12 col-md-12">

    <div class="col-sm-12 col-md-12">

      <?php echo form_open(site_url('cart/update/')); ?>

        <table class="table">

          <thead class="background-color-c1272d color-fff">

            <th>Item Description</th>

            <th >Start Date</th>

            <th >End Date</th>

            <th >Item Price</th>

            <th > Quantity </th>

            <th >Sub-Total</th>

            <th></th>



          </thead>

          <tbody>



          <?php $i = 1; ?>

          <?php foreach ($this->cart->contents() as $items): ?>

            <?php $options = $this->cart->product_options($items['rowid']);?>

            <tr <?php echo ($options['waitingList'] == true)? "class='bg-danger' title='waitingList'":'';?> >

              <?php echo form_hidden('rowid_'.$i, $items['rowid']); ?>

              <td>

                <a href="<?php echo site_url('product/rent/'.bml_urlencode($items['name']).'/'.$items['rowid']);?>"><?php echo $items['name']; ?><br />

              </td>

              <td>

                <?php echo $options['startDate']; ?><br />

              </td>

              <td>

                <?php echo $options['endDate']; ?><br />

              </td>

              <td >

                <span class="text-color"><i class="fa fa-inr"></i></span> <span id="price_<?php echo $items['rowid']; ?>"><?php echo $this->cart->format_number($items['price']); ?></span>

              </td>

              <td>

                <?php $qty=$items['qty']; ?>

                <a onclick="decrementData('<?php echo $items['rowid']; ?>')"><b class="text-danger cursor-pointer" ><i class="fa fa-minus-square"></i></b></a> &nbsp;

                <span id="qty_<?php echo $items['rowid']; ?>"> <?php echo $qty; ?> </span>  &nbsp;

                <a onclick="incrementData('<?php echo $items['rowid']; ?>')"><b class="text-success cursor-pointer" ><i class="fa fa-plus-square"></i></b></a>

              </td>

              <td >

                <span class="text-color"><i class="fa fa-inr"></i></span> <span id="subtotal_<?php echo $items['rowid']; ?>"><?php echo $this->cart->format_number($items['subtotal']); ?></span>

              </td>

              <td><a href="<?php echo site_url('cart/remove/'.$items['rowid']); ?>" ><span class="text-color"><i class="fa fa-trash"></i></span></a></td>

            </tr>

            <?php $i++; ?>

            <?php endforeach; ?>

          </tbody>

        </table>

      </div>

      <div class="col-sm-12 col-md-12">

        <hr class="hr1">

        <?php echo form_close();?>

        <a href="<?php echo site_url('welcome'); ?>"><button class="btn btn-primary">Continue Shopping</button></a>

        <div class="float-right" ><a href="<?php echo site_url('cart/clear'); ?>"><?php echo form_submit('cart/clear', 'Clear your Cart','class="btn btn-warning"'); ?></a></div>

      </div>



      <div class="clearfix"></div>

        <br>

        <div class="col-sm-3 col-md-3 border-2px-solid-999 border-radius-5px">

          <h5 class="text-center"><strong>DISCOUNT CODES</strong></h5>

          <form method="post" id="formDiscount" action="<?php echo site_url('cart/applyDiscount');?>" role="form">

            <div class="form-group">

              <input type="text" class="form-control width-100"  name="coupon" placeholder="Enter Coupon code">

              <span class="text-danger" id="couponError">

              <?php if($this->cart->getDiscountCode() != "") { ?>

              <div class="bg-success">

                <span class="text-info" id="couponLabel">

                  <?php echo $this->cart->getDiscountCode();?>

                </span>

                <button type="submit" id ="discountAction" value="clear" class="btn-transparent text-info pull-right">clear<i class="fa fa-times"></i></button>

              </div>

              <?php } ?>

            </div>

            <div class="form-group">

              <input type="hidden" name="discountAction" value=""/>

              </span>

            </div>

            <div class="form-group">

              <button type="submit" id ="discountAction" value="apply" class="btn btn-info fa "> Apply </button>

            </div>

          </form>

        </div>

        <div class="col-sm-4 col-md-4"></div>

        <div class="col-sm-5 col-md-5 text-right line-height-26px border-2px-solid-999 border-radius-5px padding-bottom-5px">

          <table class="table ">

            <tbody>

              <tr>

                <td>Sub Total :<br><span class="text-danger font-size-11px" >(excluding products in waitingList)</span></td>

                <td>

                  <span class="text-color"><i class="fa fa-inr"></i></span>

                  <span id="subtotal"><?php echo $this->cart->format_number($this->cart->subtotal()); ?></span>

                </td>

              </tr>

              <tr>

                <td>Discounts :</td>

                <td>

                  <span class="text-color"><i class="fa fa-inr"></i></span>

                  <span id="discount"><?php echo $this->cart->format_number($this->cart->cartDiscount()); ?></span>

                </td>

              </tr>

              <tr>

                <td><b>Grand Total :<br><span class="text-danger font-size-11px" >(excluding products in waitingList)</span></b></td>

                <td>

                  <b><span class="text-color"><i class="fa fa-inr"></i></span>

                  <span id="total"><?php echo $this->cart->format_number($this->cart->total()); ?></span></b>

                </td>

              </tr>

            </tbody>

          </table>

          <a href="<?php echo site_url('cart/checkout/'); ?>"><button class="btn btn-success">Proceed to Checkout</button></a>

        </div>

      </div>

<div class="col-sm-2 col-md-2"></div>

<div class="clearfix"></div>

<br>

<script type="text/javascript">

function incrementData (rowid, waiting) {



  waiting = (waiting == undefined) ? 0: waiting;

  qty = $('#qty_'+rowid).html();

   $.ajax({

            type: 'post',

            url: '<?php echo site_url('cart/incQty'); ?>',

            data: 'rowid='+rowid+'&qty='+qty+'&waiting='+waiting,

            success: function (data) {

              var data = jQuery.parseJSON(data);

              if(data.status == "success"){

                 $('#qty_'+rowid).html(data.qty);

                 price = Number($('#price_'+rowid).html().replace(/[^0-9\.]+/g,""));

                 $('#subtotal_'+rowid).html($.number(data.qty*price), 2);

                 $('#subtotal').html(data.subtotal);

                 $('#discount').html(data.discount);

                 $('#total').html(data.total);



              }

              else{

                alert(data.message);

              }

            }

        });



}



function decrementData(rowid, waiting)

{

  waiting = (waiting == undefined) ? 0: waiting;

  qty = $('#qty_'+rowid).html();

  if(qty>1)

  {

     $.ajax({

            type: 'post',

            url: '<?php echo site_url('cart/decQty'); ?>',

            data: 'rowid='+rowid+'&qty='+qty+'&waiting='+waiting,

            success: function (data) {

              var data = jQuery.parseJSON(data);

              if(data.status == "success"){

                 $('#qty_'+rowid).html(data.qty);

                 price = Number($('#price_'+rowid).html().replace(/[^0-9\.]+/g,""));

                 $('#subtotal_'+rowid).html($.number(data.qty*price), 2);

                 $('#subtotal').html(data.subtotal);

                 $('#discount').html(data.discount);

                 $('#total').html(data.total);

              }

              else{

                alert(data.message);

              }

            }

        });

   }

   else

   {

    alert("Quantity must greater than or equal to 1");

   }



}

</script>



