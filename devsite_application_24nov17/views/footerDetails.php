<div class="footer" style="background-color:#e6e6e6">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="block-title">
                        <strong>
                       <span style="font-family:inherit; font-size:24px;">
                           Follow Us
                        </span>
                    </strong>
                    </div>
                    <ul class="social-icon-list">
                        <li class="social-links facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="social-links youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li class="social-links instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li class="social-links google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li class="social-links linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="social-links twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <div class="block-title">
                        <strong>
                              <span style="font-family: inherit; font-size: 24px;">
                                  Get In Touch
                                </span>
                            </strong>
                    </div>
                    <div class="block-content">
                        <ul class="contact-info">
                            <li class="contact-form"><i class="fa fa-map-marker" style="font-size:18px;"></i>
                                <p style="margin-left: 10px;"><strong>Address:&nbsp;</strong>BookMyLens,No 745, 18th Main,1st A Cross 6th Block,Koramangala,Bangalore 560 095.</p>
                            </li>
                            <li class="contact-form"><i class="fa fa-phone" style="font-size:18px;"></i>
                                <p style="margin-left: 10px;"><strong>Phone:&nbsp;</strong>1800-121-0446</p>
                            </li>
                            <li class="contact-form"><i class="fa fa-mobile-phone" style="font-size:24px;"></i>
                                <p style="margin-left: 12px;"><strong>Phone:&nbsp;</strong>+91 9611234528</p>
                            </li>
                            <li class="contact-form"><i class="fa fa-envelope" style="font-size:18px;"></i>
                                <p style="margin-left: 10px;"><strong>Email:&nbsp;</strong><a href="mailto:mail@example.com">rentals@bookmylens.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="block-title">
                        <strong>
                             <span style="font-family: inherit; font-size: 24px;">
                                 Newsletter
                                </span>
                            </strong>
                    </div>

                    <form id="formnews" class="formnews" method="post" data-id="3571" data-name="Default sign-up form">
                        <div class="form-fields">
                            <p>
                                <input type="email" name="EMAIL" placeholder="Your email address" required="required" class="form-control" style=" width: 100%; height: 37px;border-radius: 5px; ">
                            </p>
                            <p>
                                <input type="submit" value="Subscribe" class="btn btn-outline-danger">
                            </p>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <span style="margin-top: 10px;">©Copyright 2017 by <a href="www.bookmylens.com">Bookmylens</a>. All Rights Reserved.
    	</span>
            </div>
        </div>
    </div>
</div>