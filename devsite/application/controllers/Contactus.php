<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {


	public function index()
	{
		$this->load->view('head');
		$category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$this->load->view('header',$data);
		$this->load->view('contactus');
		$this->load->view('footer_analytics');
	}
	public function app()
	{
		$this->load->view('head');
		$this->load->view('contactus');
	}
}
