<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 class Send_sms
  {
    private $CI;
    
  
    public function __construct()
    {
      $this->CI =& get_instance();
     
    }

public function sendRegistrationSMS($phone,$otp){

//Your authentication key
$authKey = "172129ARXClwsRv59a50087";

//Multiple mobiles numbers separated by comma
$mobileNumber = $phone;

//Sender ID,While using route4 sender id should be 6 characters long.
$senderId = "BMLENS";

//Your message to send, Add URL encoding here.
//$message = urlencode("Test message");
$message=$otp;
//Define route 
$route = 4;
//Prepare you post parameters
$postData = array(
    'authkey' => $authKey,
    'mobiles' => $mobileNumber,
    'message' => $message,
    'sender' => $senderId,
    'route' => $route
);

//API URL
$url="http://api.msg91.com/api/sendhttp.php";

// init the resource
$ch = curl_init();
curl_setopt_array($ch, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => $postData
    //,CURLOPT_FOLLOWLOCATION => true
));


//Ignore SSL certificate verification
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
$output = curl_exec($ch);

//Print error if any
if(curl_errno($ch))
{
    echo 'error:' . curl_error($ch);
}

curl_close($ch);

return $output;
}

public function sendOrderApprovalSuccessSms($phone,$message)
{
 return $this->sendRegistrationSMS($message);
}
public function sendOTP($phone,$otp)
{
  return $this->sendRegistrationSMS($phone,$otp);
}
public function sendOrderApprovalSms($sendSms, $suborderID)
{
    $sql = "SELECT `customerNumber`, `firstName`, `emailId`,`mobileNumber` FROM `customer` WHERE `customerId` = ( SELECT `customerId` FROM `ordertb` WHERE `orderId` = ( SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '".$suborderID."'))";
    
    $res = $this->CI->bml_database->getResults($sql);
    $res = $res[0][0];
    $data['email']=$res['emailId'];
    $phone=$res['mobileNumber'];
    $to=$res['emailId'];
    $data['firstName']=$res['firstName'];
    $sendSms = (object)$sendSms;
    $data['subOrderItems']=$sendSms->subOrderItems;
    $data['total']=$sendSms->total;
    $data['shipCost']=$sendSms->shipCost;
    $subject= "Order Approved | ".$res['customerNumber'];
    $message="This is to confirm your rental booking is approved !";
return $this->sendRegistrationSMS($phone,$message);
} 
public function sendOrderCompleteSms($sendSms, $suborderID)
{
    
    $sql = "SELECT `customerNumber`, `firstName`, `emailId`,`mobileNumber` FROM `customer` WHERE `customerId` = ( SELECT `customerId` FROM `ordertb` WHERE `orderId` = ( SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '".$suborderID."'))";
     
    $res = $this->CI->bml_database->getResults($sql);
    $res = $res[0][0];
    $data['email']=$res['emailId'];
    $to=$res['emailId'];
    $data['firstName']=$res['firstName'];
    $phone=$res['mobileNumber'];
    $sendSms = (object)$sendSms;
    $data['subOrderItems']=$sendSms->subOrderItems;
    
    $subject= $res['customerNumber']." | Completed";
    $message="Thanks for the order. We have received the equipments accordingly !";
return $this->sendRegistrationSMS($phone,$message);
} 
}