<table style="width:100%">
	<col width="40%">
	<col width="60%">
	<tr>
		<td><img src="<?php echo base_url('images/cp.png'); ?>" /></td>
		<td><h1>ESTIMATE</h1></td>
	</tr>
	<tr>
		<td>
			<h3>CREATIVE CAPTURE PVT LTD</h3>
			<p>NO 745,18th MAIN 1st A CROSS 6th BLOCK
				KORAMANGALA, BANGALORE 560095
				KARNATAKA
				<br>PAN # AAFCC6091B
				<br>VATNo.29911195473
			</p>
		</td>
		<td>
			<table width="100%">				
				<col width="30%">
				<col width="70%">
				<tr>
					<td>Date</td>
					<td>17-Jun-2016</td>
				</tr>
				<tr>
					<td>Invoice #</td>
					<td><?php echo "EST"."_".strtotime("now")."_".$custDetails['customerNumber'];?></td>
				</tr>
				<tr>
					<td>Customer ID</td>
					<td><?php echo $custDetails['customerNumber']?></td>
				</tr>
				<tr>
					<td>BILL TO</td>
					<td><?php echo $name ?></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><?php echo $address ?></td>
				</tr>
				<tr>
					<td>Contact</td>
					<td><?php echo (($custDetails['officeNumber'] != '') ? $custDetails['officeNumber'] : ($custDetails['mobileNumber'] != '') ? $custDetails['mobileNumber'] : $custDetails['landlineNumber'])?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<p>Kind Attn: <?php echo $custDetails['firstName']." ".$custDetails['lastName']?></p>
<table width="100%" style="border:2px solid C1272D;text-align:center">
	<tr style="border-top:2px solid C1272D">
		<th>Name</th>
		<th>Qty</th>
		<th>Per Day Price</th>
		<th>Total Price</th>
		<th>Start Date</th>
		<th>End Date</th>
	</tr>
	<?php foreach ($subOrderItems as $orderItems) { 
		$orderItemCount = count($orderItems);
	?>
          <tr style="border-top:2px solid C1272D">
            <?php
              $subOrderTotal = 0;
              $first = true;
              foreach ($orderItems as $orderItem) {
                $waitingList = ($orderItem['status'] == 6) ? "class='bg-danger' title='waitingList'":'';
                $colText = "";
                if(!$first) $colText.="</tr><tr>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".$orderItem['itemName']."</td>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".$orderItem['qty']."</td>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".$orderItem['perDayPrice']."</td>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".$orderItem['rentalTotalPrice']."</td>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".dateFromMysqlDate($orderItem['startDate'])."</td>";
                $colText.="<td $waitingList style='border-top:2px solid C1272D'>".dateFromMysqlDate($orderItem['endDate'])."</td>";
                echo $colText;
                $first = false;
              }
            ?>
          </tr>
	<?php } ?>
	<tr style="border-top:4px solid C1272D">
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><b>Total</b></td>
		<td><b><?php echo $total;?></b></td>
	</tr>
</table>
<p>
	Payment Details:<br>
	Creative Capture Pvt Ltd<br>
	Current Account No: 50200005019560<br>
	Ifsc code : HDFC0000053<br>
	HDFC Bank , KORAMANGALA
</p>
<p width="100%" style="text-align:center">
Make all cheques payable to CREATIVE CAPTURE PVT LTD<br>
Thank you for your business!
</p>