<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bengaluru extends CI_Controller {

	public function index()
	{
	$this->load->view('head');
		//read mainMenu Json
      //  $this->load->library('bml_read_json');
    $data['category'] = $this->bml_read_json->readRentMenu();
   ;
	$this->load->model('commonModel');
    $resultSet = $this->commonModel->getMainPageCategories();
  

    $data['mainPageCategory'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

    $result = $this->commonModel->getLatestDetails();
    $itemResult=$this->commonModel->getProductDetailsBySatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();
    
    $data['tableRows'] = $result;
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $data['itemResult'] = $itemResult;
	
    $homeSlide = $this->commonModel->getHomeSliderDetails();
    
    $homeSlide = (array_key_exists(0, $homeSlide))? $homeSlide[0]: array();
    
    $data['homeSlide'] = $homeSlide;
    
      // Setting location
    $locationKey="BNGLR";
    $this->user_session->setSessionVar('locationKey',$locationKey);

	$this->load->view('header',$data);
	$this->load->view('bengaluru');
	$this->load->view('footer_analytics');
    $this->load->view('footerDetails');
	}
	  
  
}
