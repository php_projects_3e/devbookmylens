<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  } 
  public function avail($sdate = null)
  {
	if($sdate == null) $sdate = date("Y-m-d");
	$edate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($sdate)) . " +3 month"));
	echo $sdate." : ". $edate;
	$sql = "call sp_get_order_product_qty_all('$sdate', '$edate')";
    $res = $this->bml_database->getResults($sql);
	$res = ($res[0]) ? $res[0] : array();
	$out = array();
	foreach($res as $row) {
		$out[$row['itemID']][] = $row;
	}
	
	$header = $out[$res[0]['itemID']];
	
	$data = array();
	$data['out'] = $out;
	$data['header'] = $header;
	$data['title'] = "Product Available Chat";
	$data['subTitle'] = "";
	$this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/avail');
    $this->load->view('admin/footer');
  }

  public function getProductDetails()
  {
    $data = array("title" => "Products ", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getProductDetails();
    $result = (array_key_exists(0, $result))? $result[0]: array();
    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/products');
    $this->load->view('admin/footer');
  }
  public function viewProductDetails($id)
  {
    $data = array("title" => "Product Details", "subTitle" => "#".$id,"sidebarCollapse" => true);
    $result = $this->adminModel->viewProductDetails($id);
  
    $itemType=$this->adminModel->getItemType();
    $brandName=$this->adminModel->getBrandName();
    $categoryName=$this->adminModel->getCategoryName();
    $priceResult=$this->adminModel->getPriceByID($id);

    $result = (array_key_exists(0, $result))? $result[0]: array();
   
    if(empty($result)) {
		redirect(admin_url('products/getProductDetails'));
	}
	
  $data['tableRows'] = $result;

 
    $itemType = (array_key_exists(0, $itemType))? $itemType[0]: array();
    $data['itemType'] = $itemType;
    $brandName = (array_key_exists(0, $brandName))? $brandName[0]: array();
    $data['brandName'] = $brandName;
    $categoryName = (array_key_exists(0, $categoryName))? $categoryName[0]: array();
    $data['categoryName'] = $categoryName;
    $priceResult = (array_key_exists(0, $priceResult))? $priceResult[0]: array();
    $data['priceResult'] = $priceResult;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/productsDetails');
    $this->load->view('admin/footer');
  }
  public function updateProductDetails()
  {
    $itemId=$this->input->post('itemId');
  	$days = $this->input->post('days');
    $price = $this->input->post('price');
    $sql = "";
    if(!empty($days))
    {
      foreach ($days as $id => $value) {
        if(strpos($id,"new") >-1 && $value != '' && $value > 0)
        {
          $sql .="INSERT INTO `pricemaster`(`itemId`, `days`, `price`, `seasonId`)
                  VALUES ('$itemId', '$value', '".$price[$id]."', 0);";
        }
        else{
          $sql .="UPDATE `pricemaster` SET `days`='$value',`price`='".$price[$id]."',`seasonId`=0 WHERE `priceId` = $id;";
        }
      }
    }
    $itemSpecifi=$this->db->escape($this->input->post('itemSpecifi'));
    $itemname=$this->db->escape($this->input->post('itemname'));
    $itemType=$this->db->escape($this->input->post('itemType'));
    $category=$this->db->escape($this->input->post('category'));
    $itemBrand=$this->db->escape($this->input->post('itemBrand'));
    $blrstocks=$this->db->escape($this->input->post('blrstocks'));
    $mysrstocks=$this->db->escape($this->input->post('mysrstocks'));
    $itemstatus=$this->db->escape($this->input->post('itemstatus'));
    $keyblr=$this->db->escape($this->input->post('BNGLR'));
    $keymysr=$this->db->escape($this->input->post('MYSR'));

    $itemDescription=$this->db->escape($this->input->post('itemDescription'));
   // $this->adminModel->updateProductDetails($itemId,$itemSpecifi,$itemname,$itemType,$category,$itemBrand,$itemstocks,$itemstatus,$itemDescription);
    $this->adminModel->updateProductQtyBlrDetails($itemId, $blrstocks, $keyblr);
    $this->adminModel->updateProductQtyMysrDetails($itemId, $mysrstocks, $keymysr);
    $this->bml_database->getResults($sql);
    header('Location: '.$this->agent->referrer());
  }
  public function addProduct()
  {
    $data = array("title" => "ADD NEW PRODUCT", "subTitle" => "","sidebarCollapse" => true);
    $itemType=$this->adminModel->getItemType();
    $brandName=$this->adminModel->getBrandName();
    $categoryName=$this->adminModel->getCategoryName();
    $sql ="SELECT * FROM location";
    $result= $this->bml_database->getResults($sql);
    $location_details = (array_key_exists(0, $result))? $result[0]: array();
    $data['location'] = $location_details;
    $itemType = (array_key_exists(0, $itemType))? $itemType[0]: array();
    $data['itemType'] = $itemType;
    $brandName = (array_key_exists(0, $brandName))? $brandName[0]: array();
    $data['brandName'] = $brandName;
    $categoryName = (array_key_exists(0, $categoryName))? $categoryName[0]: array();
    $data['categoryName'] = $categoryName;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/addProduct');
    $this->load->view('admin/footer');

  }
  public function deleteProductByID($id)
  {
    $result = $this->adminModel->deleteProductByID($id);
    redirect(admin_url('products/getProductDetails'));
  }
  public function insertProductDetails()
  {
    $itemType=$this->db->escape($this->input->post('itemType'));
    $itemBrand=$this->db->escape($this->input->post('itemBrand'));
    $itemCategory=$this->db->escape($this->input->post('category'));
    $itemDescription=$this->db->escape($this->input->post('itemDescription'));
    $itemImage1=$itemImage2='';
    $itemName=$this->db->escape($this->input->post('itemname'));
    $itemStockBlr=$this->db->escape($this->input->post('Bengaluru-place'));
    $itemStockMysr=$this->db->escape($this->input->post('Mysuru-place'));
    $itemBlrKey = $this->db->escape($this->input->post('Bengaluru-key'));
    $itemMysrKey = $this->db->escape($this->input->post('Mysuru-key'));
    
    
    $itemStatus=$this->db->escape($this->input->post('itemstatus'));
    $itemSpecification=$this->db->escape($this->input->post('itemSpecifi'));
   
  
    
    if($_FILES['itemImage1']['name'])
   {
      $uploadItemImage['upload_path'] = $this->config->config['upload_item_path'];
      $uploadItemImage['allowed_types'] = 'jpg|png';
      $uploadItemImage['override'] = true;
      $this->load->library('upload', $uploadItemImage);
      $this->upload->do_upload('itemImage1');
      $itemImage1 = ($this->upload->data()['file_name']);
    }
	if($_FILES['itemImage2']['name'])
    {
      $uploadItemImage['upload_path'] = $this->config->config['upload_item_path'];
      $uploadItemImage['allowed_types'] = 'jpg|png';
      $uploadItemImage['override'] = true;
      $this->load->library('upload', $uploadItemImage);
      $this->upload->do_upload('itemImage2');
	  $itemImage2 = ($this->upload->data()['file_name']);
    }
    
    $result = $this->adminModel->insertProductDetails($itemType,$itemBrand,$itemCategory,$itemDescription,$itemImage1,$itemImage2,$itemName,$itemStatus,$itemSpecification);
    $itemId= $this->db->insert_id();
    $this->user_session->setSessionVar('id',$itemId);
    $item = $this->user_session->getSessionVar('id');
    $query = $this->db->query('SELECT LAST_INSERT_ID()');
    $row = $query->row_array();
    $LastIdInserted = $row['LAST_INSERT_ID()'];
    
   
    if($itemStockBlr!="NULL" && $itemStockMysr!="NULL")
    {
     
      $result_gen=  $this->adminModel->insertLocationqtyDetails($itemStockBlr, $itemStockMysr,$itemBlrKey,$itemMysrKey,$LastIdInserted); 
   
    } else if($itemStockBlr=="NULL") {
 
    $result_mysr=  $this->adminModel->insertMysrqtyDetails($itemStockMysr,$itemBlrKey,$itemMysrKey,$LastIdInserted);
     
    }  else {

      $result_blr=  $this->adminModel->insertBlrqtyDetails($itemStockBlr,$itemBlrKey,$itemMysrKey,$LastIdInserted); 

   }
    redirect(admin_url('products/viewProductDetails/').$LastIdInserted);
    
  }
  public function insertItemType()
  {
    $itemTypeName=$this->input->post('itemtypename');
    $result = $this->adminModel->insertItemType($itemTypeName);
    header('Location: '.$this->agent->referrer());
  }
  public function insertBrandType()
  {
    $itemBrandName=$this->input->post('itembrandname');
    $result = $this->adminModel->insertBrandType($itemBrandName);
    header('Location: '.$this->agent->referrer());
  }
  public function insertCategoryType()
  {
    $category_Name=$this->input->post('itemcategoryname');
    $displayOrder=$this->input->post('categoryorder');
    $result = $this->adminModel->insertCategoryType($category_Name,$displayOrder);
    header('Location: '.$this->agent->referrer());
  }
  public function updateItemImage()
  {
    $itemId=$this->input->post('itemId');
    if(isset($_FILES['itemimage1']) && $_FILES['itemimage1']['name'])
    {
      $uploadItemImage['upload_path'] = $this->config->config['upload_item_path'];
      $uploadItemImage['allowed_types'] = 'jpg|png';
      $uploadItemImage['override'] = true;
      $this->load->library('upload', $uploadItemImage);
      $this->upload->do_upload('itemimage1');
      $itemImage = $this->upload->data()['file_name'];
      $this->adminModel->updateItemImage($itemImage,null,$itemId);
      $this->upload->display_errors();
    }
    if(isset($_FILES['itemimage2']) && $_FILES['itemimage2']['name'])
    {
      $uploadItemImage['upload_path'] = $this->config->config['upload_item_path'];
      $uploadItemImage['allowed_types'] = 'jpg|png';
      $uploadItemImage['override'] = true;
      $this->load->library('upload', $uploadItemImage);
      $this->upload->do_upload('itemimage2');
      $itemImage = ($this->upload->data()['file_name']);
      $this->adminModel->updateItemImage(null,$itemImage,$itemId);
      $this->upload->display_errors();
    }
    header('Location: '.$this->agent->referrer());

  }

  public function getLatestDetails()
  {
    $data = array("title" => "Latest Products ", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getLatestDetails();
    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $data['itemResult'] = $itemResult;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/latest');
    $this->load->view('admin/footer');
  }
  public function removeLatest($id)
  {
    $this->adminModel->removeLatest($id);
    header('Location: '.$this->agent->referrer());
  }
  public function insertLatestItem()
  {
    $itemID=$this->input->post('itemID');
    $this->adminModel->insertLatestItem($itemID);
    header('Location: '.$this->agent->referrer());
  }
  public function getHomeSliderDetails()
  {
    $data = array("title" => "Home Slider Products ", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getHomeSliderDetails();
    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $data['itemResult'] = $itemResult;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/homeSlide');
    $this->load->view('admin/footer');

  }
  public function addHomeSliderDetails()
  {
    $data = array("title" => "ADD NEW HOME SLIDE", "subTitle" => "","sidebarCollapse" => true);

    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $itemList = array();
    foreach ($itemResult as $itemListRow) {
      $itemList[] = array(
                          'label' => str_replace('/', "'", $itemListRow['itemName'])
                      );
    }
    $data['itemList'] = $itemList;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/addHomeSlider');
    $this->load->view('admin/footer');

  }
  public function insertHomeSlideDetails()
  {
    $slidetitle=$this->input->post('slidetitle');
    $slidecontent=$this->input->post('slidecontent');
    $slideBGImage='';
    $slideItemImage='';
    $slideItemName=$this->input->post('slideItemID');
    $sql='SELECT `itemId` FROM `itemmaster` WHERE itemName ="'.$slideItemName.'"';
    $slideItemID = $this->bml_database->getResults($sql);

    $slideItemID = $slideItemID[0][0]['itemId'];
    if($_FILES['slideBGImage']['name'])
    {
      $uploadslideBGImage['upload_path'] = $this->config->config['upload_slide_path'];
      $uploadslideBGImage['allowed_types'] = '*';
      $uploadslideBGImage['override'] = true;
      $this->load->library('upload', $uploadslideBGImage);
      $this->upload->do_upload('slideBGImage');
      $slideBGImage = $_FILES['slideBGImage']['name'];
    }
    if($_FILES['slideItemImage']['name'])
    {
      $uploadslideItemImage['upload_path'] = $this->config->config['upload_slide_path'];
      $uploadslideItemImage['allowed_types'] = '*';
      $uploadslideItemImage['override'] = true;
      $this->load->library('upload', $uploadslideItemImage);
      $this->upload->do_upload('slideItemImage');
      $slideItemImage = $_FILES['slideItemImage']['name'];
    }

    $result = $this->adminModel->insertHomeSlideDetails($slidetitle,$slidecontent,$slideBGImage,$slideItemImage,$slideItemID);

    redirect(admin_url('products/viewHomeSlider/').$this->db->insert_id());
  }
  public function viewHomeSlider($id)
  {
    $data = array("title" => "Home Slide Details", "subTitle" => "#".$id,"sidebarCollapse" => true);
    $result = $this->adminModel->getHomeSliderDetailsById($id);
    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $itemList = array();

    $data['tableRows'] = $result;
    $data['itemResult'] = $itemResult;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/homeSlideDetails');
    $this->load->view('admin/footer');
  }
  public function updateHomeSlideItemImage()
  {
    $itemId=$this->input->post('popupId');
    if($_FILES['slideItemImage']['name'])
    {
      $uploadslideItemImage['upload_path'] = $this->config->config['upload_slide_path'];
      $uploadslideItemImage['allowed_types'] = '*';
      $uploadslideItemImage['override'] = true;
      $this->load->library('upload', $uploadslideItemImage);
      $this->upload->do_upload('slideItemImage');
      $slideItemImage = $_FILES['slideItemImage']['name'];
      $this->adminModel->updateHomeSlideItemImage($slideItemImage,$itemId);
      $this->upload->display_errors();
    }
    header('Location: '.$this->agent->referrer());

  }
  public function updateHomeSlideBGImage()
  {
    $itemId=$this->input->post('popupId');
    if($_FILES['slideBGImage']['name'])
    {
      $uploadslideBGImage['upload_path'] = $this->config->config['upload_slide_path'];
      $uploadslideBGImage['allowed_types'] = '*';
      $uploadslideBGImage['override'] = true;
      $this->load->library('upload', $uploadslideBGImage);
      $this->upload->do_upload('slideBGImage');
      $slideBGImage = $_FILES['slideBGImage']['name'];
      $this->adminModel->updateHomeSlideBGImage($slideBGImage,$itemId);
      $this->upload->display_errors();
    }
    header('Location: '.$this->agent->referrer());

  }

  public function removeHomeSlider($id)
  {
    $this->adminModel->removeHomeSlider($id);
    header('Location: '.$this->agent->referrer());
  }
   public function getFeaturedDetails()
  {
    $data = array("title" => "Featured Enquipment", "subTitle" => "","sidebarCollapse" => true);
    $result = $this->adminModel->getFeaturedDetails();
    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();
    $data['itemResult'] = $itemResult;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/featured_equipment');
    $this->load->view('admin/footer');
  }
  public function removeFeatured($id)
  {
    $this->adminModel->removeFeatured($id);
    header('Location: '.$this->agent->referrer());
  }
  public function insertFeaturedItem()
  {
    $itemID=$this->input->post('itemID');
    $this->adminModel->insertFeaturedItem($itemID);
    header('Location: '.$this->agent->referrer());
  }

}
