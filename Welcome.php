<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('head');
		//read mainMenu Json
    $category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$this->load->view('header',$data);
		$this->load->view('footer');
	}
}
