<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyaccountModel extends CI_Model
{

  public function __construct()
  {
      parent::__construct();
  }

  public function isRegistartionComplete()
  {
	$sql = "SELECT customerId, `pancardNumber`, `addressProof`, `idProof`, `additionalFile1`, `additionalFile2`, `customerImagePath`  FROM `customer` WHERE `emailId` = '".$this->user_session->getSessionVar('emailID')."'";
	$res = $this->bml_database->getResults($sql);
	//print_r($res);
	
	$res = (array_key_exists(0, $res) ? $res[0][0] : -1);
	if($res == -1) return false;
	if($res['pancardNumber'] == '' || $res['pancardNumber'] == 'customerImagePath') return false;
	$dCount=0;
	if($res['addressProof'] != '' ) $dCount++;
	if($res['idProof'] != '' ) $dCount++;
	if($res['additionalFile1'] != '' ) $dCount++;
	if($res['additionalFile2'] != '' ) $dCount++;
	if($dCount < 2) return false;
	$dCount = 0;
	
	$customerId = $res['customerId'];
	$sql = "SELECT count(*) as count FROM `customer_address` WHERE `customerId` = '".$customerId."'";
	$res = $this->bml_database->getResults($sql);
	$res = (array_key_exists(0, $res) ? $res[0][0] : -1);
	if($res == -1) return false;
	
	if($res['count'] < 1 ) return false;
	return true;
  }
  public function updateProducts()
{
$sql= "SELECT customerId FROM customer WHERE 1";

return $this->bml_database->getResults($sql);

}
public function updateProductids($result)
{
foreach($result as $id)
{
$productid=$id['customerId'];

$sql= "UPDATE customer SET locationKey='BNGLR'  WHERE CustomerID=$productid";

$this->bml_database->getResults($sql);

}
}
  public function insertFBUser($id, $email, $fbURL, $picSquare)
  {
	$sql = "INSERT INTO `facebook`(`oauth_uid`, `email`, `pic_square`, `facebookURL`) 
			VALUES ('$id', '$email', '$fbURL', '$picSquare')";
	return $this->bml_database->getResults($sql);
  }
  /* -----------inserting image------ */
  function insertFacebookImage($emailId,$path)
  {
    $sql = "UPDATE `customer` SET customerImagePath= '$path' WHERE emailId= '$emailId' ";
    return $this->bml_database->getResults($sql);
  }
  /* ------------- */
  public function registerUser($companyPerson, $fname,$lname,$gender, $emailId,$pwd1,$phone,$locationKey)
  {
    $sql = "call sp_register_user('$companyPerson','$fname','$lname','$gender', '$emailId','$pwd1','$phone','$locationKey')";
    return $this->bml_database->getResults($sql);

  }
  public function tempUser($companyPerson, $fname,$lname,$gender, $emailId,$pwd1,$phone,$otp,$locationKey)
  {
      $data=array(
            'firstname'=>$fname,
            'lastname'=>$lname,
            'gender'=>$gender,
            'companyPerson'=>$companyPerson,
            'emailId'=>$emailId,
            'password'=>$pwd1,
            'mobileNumber'=>$phone,
            'otpNumber'=>$otp,
            'locationKey'=>$locationKey
      );
      $query = $this->db->query("SELECT * FROM `tmp_customer` WHERE `emailId`='".$emailId."'"); 
   //$query="SELECT * FROM `tmp_customer` WHERE `emailId`='".$emailId."'";
      if($query->num_rows() == 0 ){
        $sql=$this->db->insert('tmp_customer',$data);    
      }else{
         /*  $sql = "UPDATE tmp_customer
                  SET otpNumber='$otp'
                  where emailId = '$emailId'
                  and mobileNumber='$phone'"; */
          $sql= $this->db->set('otpNumber', $otp)
                        ->set('mobileNumber',$phone)
                        ->set('firstname',$fname)
                        ->set('lastname',$lname)
                        ->set('companyPerson',$companyPerson)
                        ->set('gender',$gender)
                        ->set('password',$pwd1)
                        ->set('locationKey',$locationKey)
                        ->where('emailId',$emailId)                       
                        ->update('tmp_customer');
      } 
      return $this->bml_database->getResults($sql);
  }
/*-----------user OTP-----------  */
public function userOTP($emailId,$otp)
{
  $sql="INSERT INTO  `otp_expiry`(`email`,`otp`,`expired`,`create`) 
              VALUES ('$emailId','$otp', 0,date('Y-m-d_H-i'))";
             // return true;
  return $this->bml_database->getResults($sql);
}
public function getUserOTP($emailId){
  //$sql="SELECT otpNumber FROM `temp_user` where emailId='$emailId'";
  $sql=$this->db->select('otpNumber')->from('tmp_customer')->where('emailId',$emailId)->get();
  //return $this->bml_database->getResults($sql);
  /*
  * return rows in form of arrays
  */
  //return $sql->result_array();
  /* 
  * return single result row and first row in table
  * row() return objects
  * row_array() return objects
  */
  /**
   * If  Email-Id has more than one otp then
   * select last row else 
   * single row in array form
   */
   if($sql->num_rows() > 1) {
            return $sql->last_row('array');
    }
   //  return $sql->last_row(‘array’);
    return $sql->row_array();

}
public function getUserDetails($user_email,$entered_otp_number)
{
    $array=array(
            'emailId'=>$user_email,
            'otpNumber'=>$entered_otp_number
    );
    $sql=$this->db->select('*')->from('tmp_customer')->where($array)->get();
    /**
   * If  Email-Id has more than one otp then
   * select last row else 
   * single row in array form
   */
   if($sql->num_rows() > 1) {
            return $sql->last_row('array');
    }
    return $sql->row_array();

}
public function updateOTP($emailId,$otp_number){
  
  $sql = "UPDATE tmp_customer
              SET otpNumber='$otp_number'
              where emailId = '$emailId'";  
  
 
  return $this->bml_database->getResults($sql);
}
//public function verifiedOTP($number){
  public function verifiedOTP($emailId){
 // $sql="SELECT a.emailId,b.pwd1 FROM `otp_expiry` a JOIN `customer` b where otp=$number and expired=0 ";
 $sql="SELECT a.email,b.pwd1 FROM `otp_expiry` a JOIN `customer` b on a.email=b.emailId where email=$emailId";
     $result = $this->bml_database->getResults($sql);
   /*  $status = (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
    if($status && $result[0][0]['expired'] == 0)
    {
      $sql =  "UPDATE `otp_expiry` SET expired=1";
     
    }
    else{
      echo "otp expired";
    } */
   

}
/*-------------------- */
  public function checkEmailExists($emailID){
    $sql = "select count(1) as count from customer where emailId = '$emailID'";
    $result = $this->bml_database->getResults($sql);
    return (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
  }
  public function checkMobileNumberExists($phone){
       $sql = "select count(1) as count from customer where mobileNumber = '$phone'";
    $result = $this->bml_database->getResults($sql);
    return (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
  }

  public function validateLogin($emailID, $psd){
    
	$sql = "select count(1) as count from customer where emailId = '$emailID' and loginPassword = '$psd'";
    $result = $this->bml_database->getResults($sql);
    $status = (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
	if($status)	return $status;
	
	$psd = md5($psd);
    $sql = "select count(1) as count from customer where emailId = '$emailID' and loginPassword = '$psd'";
    $result = $this->bml_database->getResults($sql);
    return (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
				  
  }

  public function validateFBLogin($emailID, $accountID){
    $sql = "select count(1) as count from customer where emailId = '$emailID'; SELECT count(1) as count FROM `facebook` WHERE `email` = '$emailID' and `oauth_uid` = '$accountID';";
    $result = $this->bml_database->getResults($sql);
    $isAccountExist = (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
	if(!$isAccountExist)
	{
		return "NOACCOUNT";
	}
	else
	{
		$isFBAccountExist = (array_key_exists(1, $result)
              && array_key_exists(0, $result[1])
                && array_key_exists('count', $result[1][0])
                  && $result[1][0]['count'] > 0);
		if($isFBAccountExist)
		{
			return "FBACCOUNT";
		}
		else
		{
			return "BMLACCOUNT";
		}
	}
  }

  public function validateEmailCode($email, $code)
  {
    $sql = "select count(1) as count,e.`status` as confirmationStatus from customer c, tbl_email_confirmation e where c.customerId = e.customerID and c.emailId = '$email' and e.confirmationCode = '$code'";
    $result = $this->bml_database->getResults($sql);
    $status = (array_key_exists(0, $result)
              && array_key_exists(0, $result[0])
                && array_key_exists('count', $result[0][0])
                  && $result[0][0]['count'] > 0);
    if($status && $result[0][0]['confirmationStatus'] == 0)
    {
      $sql = "UPDATE customer c, `tbl_email_confirmation` e
              SET e.`status`=1,c.emailFlag = 1
              where c.customerId = e.customerID
              and c.emailId = '$email'
              and e.confirmationCode = '$code'";
      $result = $this->bml_database->getResults($sql);
      $status = 'SUCCESS';
    }
    else if($result[0][0]['confirmationStatus'] == 1)
    {
      $status = "EXPIRE";
    }
    return $status;
  }
  public function getUserSessionDetails()
  {
    $sql= "SELECT `customerId`,`customerNumber`,`accountType`, `firstName`,`lastName`,`status`,`locationKey` FROM `customer`WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $result = $this->bml_database->getResults($sql);

    return $result[0][0];
  }

  public function checkTermsFlag()
  {
    $sql="SELECT `termsFlag` FROM `customer` WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $result = $this->bml_database->getResults($sql);
    $termStatus=$result[0][0]['termsFlag'];
    return $termStatus;
  }
  public function updateTermsFlag()
  {
    $sql="UPDATE `customer` SET `termsFlag`=1 WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $this->bml_database->getResults($sql);
  }
  public function getCustomerInfo()
  {
    $sql="SELECT * FROM `customer` WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'; SELECT * FROM `customer_address` WHERE `customerId`='".$this->user_session->getSessionVar('customerId')."'";
    return $this->bml_database->getResults($sql);
  }
  public function updateCompanyInfo($companyPerson , $cAddressID)
  {
    $sql="UPDATE `customer` SET `companyPerson`='".$companyPerson."',`companyID`='".$cAddressID."' WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $this->bml_database->getResults($sql);
	
  }
  public function updateContactInfo($mobileNumber , $landlineNumber, $officeNumber)
  {
    $sql="UPDATE `customer` SET `mobileNumber`='".$mobileNumber."',`landlineNumber`='".$landlineNumber."',`officeNumber`='".$officeNumber."', contactNumberFlag = 1, `status` = if(`status` = 1, 3, `status`) WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $this->bml_database->getResults($sql);
  }
  public function updatePanInfo($panNumber)
  {
    $sql="UPDATE `customer` SET `pancardNumber`='".$panNumber."' WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $this->bml_database->getResults($sql);
  }
  public function updateDOBInfo($dob)
  {
    $sql="UPDATE `customer` SET `DOB`='".$dob."' WHERE `emailId`='".$this->user_session->getSessionVar('emailID')."'";
    $this->bml_database->getResults($sql);
  }
  public function updateAddress($paddress, $paddress1, $pcity, $pstate, $ppincode,$plandmark,$addressType)
  {
    $sql="UPDATE `customer_address` a, customer b  SET a.`addressLine1`='".$paddress."',a.`addressLine2`='".$paddress1."',a.`addressCity`='".$pcity."',a.`addressState`='".$pstate."',a.`addressPin`='".$ppincode."',a.`addressLandmark`='".$plandmark."', b. addressFlag = 1, b.`status` = if(b.`status` = 1, 3, b.`status`), a.status = 0 WHERE `addressType`='".$addressType."' and a.`customerId`='".$this->user_session->getSessionVar('customerId')."'  and a.`customerId` = b.`customerId`";
    $this->bml_database->getResults($sql);
  }
  public function insertAddress($address, $address1, $city, $state, $pincode,$landmark,$addressType)
  {
    $sql="INSERT INTO `customer_address`(`customerId`, `addressLine1`, `addressLine2`, `addressCity`, `addressState`, `addressPin`, `addressLandmark`, `addressType`) VALUES('".$this->user_session->getSessionVar('customerId')."','".$address."', '".$address1."', '".$city."','".$state."','".$pincode."','".$landmark."','".$addressType."')";
    $this->bml_database->getResults($sql);
  }
  public function getOrderListByStatus()
    {
      $sql = "select a.`subOrderID`, b.orderId, a.`orderNumber`, c.customerNumber, concat(c.firstName, ' ', c.lastName) as customerName, a.`orderSubtotal`, a.`orderDiscountAmount`, (a.`orderSubtotal`-a.`orderDiscountAmount`) as orderTotal,e.collectDate,e.startDate, e.endDate, d.orderStatusDisplayName ,concat(f.addressLine1,',', f.addressLine2,',',f.addressCity,',',f.addressState,',',f.addressPin,',',f.addressLandmark) AS 'deleveryAddressTYPE', concat(g.addressLine1,',', g.addressLine2,',',g.addressCity,',',g.addressState,',',g.addressPin,',',g.addressLandmark)AS 'pickupAddressTYPE',e.itemId
              FROM `tbl_suborder` a
              JOIN ordertb b
              on a.orderID = b.orderId
              JOIN customer c
              on b.customerId = c.customerId
              JOIN tbl_order_status d
              on a.status = d.orderStatusID
              JOIN orderproduct e
              on a.subOrderID = e.subOrderID
              LEFT JOIN customer_address f
              on b.deliveryAddress= f.addressID
              LEFT JOIN customer_address g
              on b.pickupAddress= g.addressID
              where b.customerId = '".$this->user_session->getSessionVar('customerId')."'
              group by a.subOrderID
			  order by b.orderId desc";
		return $this->bml_database->getResults($sql);
    }
    public function insertCompanyAddress($companyName,$caddressLine1,$ccity,$cstate,$cpin,$coffice,$ctin,$cpan,$ccst)
    {
      $sql="INSERT INTO `company_details`( `name`, `addr`, `city`, `state`, `pin`, `officePhone`, `tin`, `pan`, `cst`) VALUES ('".$companyName."','".$caddressLine1."','".$ccity."','".$cstate."','".$cpin."','".$coffice."','".$ctin."','".$cpan."','".$ccst."')";
      $this->bml_database->getResults($sql);
    }
    public function updateCompanyAddress($companyAddressID,$companyName, $caddressLine1, $ccity, $cstate, $cpin,$coffice, $ctin, $cpan, $ccst)
    {
      $sql="UPDATE `company_details` SET `name`='".$companyName."',`addr`='".$caddressLine1."',`city`='".$ccity."',`state`='".$cstate."',`pin`='".$cpin."',`officePhone`='".$coffice."',`tin`='".$ctin."',`pan`='".$cpan."',`cst`='".$ccst."' WHERE `companyID`='".$companyAddressID."'";
      $this->bml_database->getResults($sql);
    }

}