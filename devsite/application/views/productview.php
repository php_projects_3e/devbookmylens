<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8">
  <div class="row">
    <div class="col-md-6">
    <?php
      $breadcrumbText = '';
      foreach ($breadcrumb as $breadcrumbRow){
        $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
        $breadcrumbText .= " > ";
      }
      echo rtrim($breadcrumbText, " > ");
    ?>
    </div>

  </div>
  <?php if(!empty($productDetails)) { ?>
  <div class = "row">
    <div class="col-md-12">
        <div class="col-md-6 border-5px-solid-C1272D margin-top-10px height-368px" >
          <img src="<?php echo site_url('images/viewImages/'.$productDetails['itemImage']);?>" alt="Images" style="width:90%"/>
          <div>
            <h1 class="text-center" style="line-height: 32px;font-size: 32px;max-height: 64px;overflow:hidden">
				<b><?php echo $productDetails['itemName'];?></b>
			</h1>
          </div>
        </div>
        <div class="col-md-6 border-5px-solid-C1272D margin-top-10px height-368px">
          <div class="full-width-tabs" >
            <ul class="tabs nav  nav-tabs margin-bottom-15px background-c1272d width-100.5 border-bottom-0px" >
              <li class="take-all-space-you-can width-33" ><a class="padding-8px-0px text-align-center" href="#overview"
                data-toggle="tab">Overview</a></li>
              <li class="active take-all-space-you-can width-33" ><a class="padding-8px-0px text-align-center" href="#pricelist"
                data-toggle="tab">Price</a></li>
              <li class="take-all-space-you-can width-33" ><a  class="padding-8px-0px text-align-center" href="#specification"
                data-toggle="tab" >Tech Spec</a>
              </li>
            </ul>

            <div id="myTabContent" class="tab-content padding-10px" >
                <div class="tab-pane fade " id="overview">
                    <p class="text-align: justify;" style="height: 220px;overflow: auto;"><?php echo $productDetails['itemDescription']; ?></p>
                </div>
                <div class="tab-pane fade active in" id="pricelist">
                    <p>
                      <table class="table table-striped table-bordered table-condensed">
                        <tr>
                          <th>No. of Days</th>
                          <th>Per Day Rates</th>
                        </tr>
                        <?php foreach ($productDetails['priceList'] as $priceList) { ?>
                          <tr>
                            <td> <?php echo $priceList['days']."+";?></td>
                            <td>
                              <?php
                                if($priceList['price']) {
                                  echo $priceList['price']."&nbsp;INR";
                                }
                                else {
                                  echo "* Contact Us";
                                }
                              ?>
                            </td>
                          </tr>
                        <?php } ?>
                      </table>
                    </p>
                </div>

                <div class="tab-pane fade" id="specification">
                  <p class="text-align: justify;" style="height: 220px;overflow: auto;"><?php echo $productDetails['itemSpecification']; ?></p>
                </div>
            </div>

            <div class="display-table bottom-13px right-0 left-0 height-40px width-100 text-align-center">
              <div class="display-table-cell vertical-align-middle margin-top-5px">
                <a id="rentlink" href="<?php echo site_url('product/rent/'.bml_urlencode($productDetails['itemName']));?>" class="button font-weight-bold font-size-18px color-EAD608 margin-left-0px  height-40px padding-top-8px" style="background: #C1272D;" >
                  Rent It
                </a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <?php } else {?>
  <h3>Product Not Found</h3>
  <?php } ?>

</div>
<div class="col-sm-2 col-md-2"></div>
