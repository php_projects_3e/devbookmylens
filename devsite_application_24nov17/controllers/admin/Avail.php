<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avail extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  }

  public function details($subOrderId) {
  
	$subOrderIds = explode (",", $subOrderId);
	$data = array();
	$data['orderDetails'] = array();
		
	foreach($subOrderIds as $id)
	{
		$result = $this->adminModel->getOrderDetailsBySuborderID($id);
		foreach($result[2] as $row)
		{
			$data['orderDetails'][$id]['items'][] = $row;
		}
		$data['orderDetails'][$id]['details'] = $result[0][0];
	}
	
	echo json_encode($data);
  }
}
?>