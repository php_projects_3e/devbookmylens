<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerateDynamicContentJson extends CI_Controller {

  /**
  * generateRentMenu
  **/

  public function index()
  {
    $this->load->library('bml_generate_json');
    $this->bml_generate_json->generateRentMenu();
    $this->bml_generate_json->generateFAQs();
  }
}
