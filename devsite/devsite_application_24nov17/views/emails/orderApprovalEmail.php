<?php 
	$arrKey = array_keys($subOrderItems); 
	$subOrderID = (count($subOrderItems) == 1)? "[".$arrKey[0]."]" :'';
?>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="750" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:calibri;" >

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:2px solid #ab2f2f; font-family:calibri;" >
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <img src="<?php echo base_url(); ?>images/bml_logo_new.png" style="max-height:50px; margin-top:-5px;">
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px;">
  <tr><td>&nbsp;</td></tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
  <td>
   <p style="font-size:18px;">Dear
      <?php
        if($firstName!="")
        {
           echo $firstName.", ";
        }
      ?>
  </p>
  <p>This is to confirm your rental booking <?php echo $subOrderID; ?> is approved for the following item
  <table   style='border:1px solid #333;padding: 5px;'>
	<thead>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Order Number</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Product Name</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Start Date</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>End Date</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Per Day Price</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Qty</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Rental Price</th>
	  <th align="center"   style='border:1px solid #333;padding: 5px;'>Sub-Total</th>
	</thead>
	<tbody>
	<?php foreach ($subOrderItems as $orderNumber => $orderItems) { $orderItemCount = count($orderItems);?>
	  <tr>
		<td rowspan="<?php echo $orderItemCount;?>"    style='border:1px solid #333;padding: 5px;'><?php echo $orderNumber;?></td>
		<?php
		  $subOrderTotal = 0;
		  $first = true;
		  $colTextArr = "";
		  foreach ($orderItems as $orderItem) {
			$waitingList = ($orderItem['status'] == 6) ? "class='bg-danger' title='waitingList'":'';
			$colText = "";
			$subOrderTotal += $orderItem['rentalTotalPrice'];
			if(!$first) $colText.="</tr><tr>";
			$colText.="<td $waitingList    style='border:1px solid #333;padding: 5px;'>".$orderItem['itemName']."</td>";
			$colText.="<td $waitingList    style='border:1px solid #333;padding: 5px;'>".dateFromMysqlDate($orderItem['startDate'])."</td>";
			$colText.="<td $waitingList    style='border:1px solid #333;padding: 5px;'>".dateFromMysqlDate($orderItem['endDate'])."</td>";
			$colText.="<td $waitingList    style='border:1px solid #333;padding: 5px;'>".($orderItem['rentalTotalPrice']/ $orderItem['duration'])."</td>";
      $colText.="<td $waitingList class='border-top-1px-solid-C1272D'>".$orderItem['qty']."</td>";
	  	$colText.="<td $waitingList    style='border:1px solid #333;padding: 5px;'>".$orderItem['rentalTotalPrice']."</td>";
			$colTextArr[] = $colText;
			$first = false;
		  }
		  $first = true;
		  foreach ($colTextArr as $colText) {
			echo $colText;
			if($first) {
			  echo "<td rowspan='".$orderItemCount."'    style='border:1px solid #333;padding: 5px;'>
						".$subOrderTotal."
					</td>";
			  $first = false;
			}
		  }
		?>
	  </tr>
	<?php } ?>
	</tbody>
  </table>
  <p>Items Amount: Rs. <?php echo round($total); ?></p>
  <p>Delivery Charge: Rs. <?php echo $shipCost; ?></p>
  <p>Total Cost : Rs. <?php echo round($total+$shipCost); ?></p>
  
  <p>&nbsp;</p>
  <p>BookMyLens Team,<br>
	Call us: 080 41633669/ 9611234528<br>
	Email Id: rentals@bookmylens.com<br>
	www.bookmylens.com<br>
	www.facebook.com/bookmylens</p>
  </td>
  </tr>
    <tr><td>&nbsp;</td></tr>
  </table>
  </td></tr>
  <tr><td align="center">
  <p style="color:grey; font-size:12px;">This welcome email is sent to <a><ins style="color:#24798e"><?php echo $email; ?></ins></a> from <a href="http://bookmylens.com" style="color:#24798e">www.bookmylens.com</a>
</p>
<p style="margin-bottom:10px;"></p>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
</body>
