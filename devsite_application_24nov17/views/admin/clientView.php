<?php $customerStatusText = array('<span class="text-info">Unapproved</span>','<span class="text-success">Approved</span>','<span class="text-danger">Blocked</span>','<span class="text-info">Info Changed</span>');?>
<?php $addressStatusButtons = array('<button name="addrButton" type="submit" value = "approve" class="btn btn-success btn-xs">Approve</button>','<button name="addrButton" type="submit" value = "unapprove" class="btn btn-danger btn-xs">Unapprove</button>');?>

<div class="box">
    <div class="box-body">
        <div class="col-lg-1 col-sm-1" >
        	<?php 	$clientPhoto=site_url('uploads/'.$customerDetails['customerImagePath']);
        			if(file_exists($clientPhoto)) { ?>
                        <img src="<?php echo site_url('uploads/'.$customerDetails['customerImagePath']); ?>" class="img-responsive" height="150px;">
                    <?php  } else { ?>
                        <img src="<?php echo site_url('images/custImage.png'); ?>" class="img-responsive">
                    <?php } ?>
        </div>
        <div class="col-lg-8 col-sm-8">
        	<div class="col-lg-6 col-md-6">
        		<div class="col-lg-12 col-md-12">
	        		<div class="col-sm-6 col-md-6 col-lg-6 ">
		        		<b>Customer Number :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['customerNumber'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>First Name :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['firstName'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Last Name :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['lastName'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Gender :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['gender'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Email ID :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<b><?php echo "<span class = '".(($customerDetails['emailFlag'] == 1) ? 'text-success': 'text-danger')."'>".$customerDetails['emailId'];?></span></b>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-7 col-md-6 col-lg-6 ">
		        	<b>DOB :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['DOB'];?>
		        	</div>
		        	<div class="clearfix"></div>
	        	</div>
	        	<div class="col-lg-9 col-sm-9 col-md-9">
		        	<div class="col-sm-7 col-md-6 col-lg-6 ">
		        	<b>Mobile No. :</b>
		        	</div>
		        	<div class="col-sm-7 col-md-6 col-lg-6 text-right">
		        		<?php echo $customerDetails['mobileNumber'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-7 col-md-6 col-lg-6 ">
		        	<b>Landline No.:</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 text-right">
		        		<?php echo $customerDetails['landlineNumber'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Office Number :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 text-right">
		        		<?php echo $customerDetails['officeNumber'];?>
		        	</div>
	        	</div>
	        	<div class="col-lg-3 col-sm-3 col-md-3 border-left padding-15px-0px">
	        		<a <?php echo ($customerDetails['contactNumberFlag']) ? "disabled":"";?> href="<?php $customerNumber=$customerDetails['customerNumber']; echo admin_url("clients/approve/mobile/$customerNumber"); ?>" class="btn tbn-transparent fa fa-check text-success"></a>
		        </div>

	        </div>

        	<div class="col-lg-6 col-md-6">
	        	<div class="col-lg-12 col-md-12">
	        		<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Last Update :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['lastUpdateDate'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Registered Date :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['registeredDate'];?>
		        	</div>
		        	
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Pan Number:</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<?php echo $customerDetails['pancardNumber'];?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Membership Ty. :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		Normal
		        	</div>
		        	<div class="clearfix"></div>
		        </div>
		        <div class="col-lg-8 col-md-8">
		        	<div class="col-sm-10 col-md-10 col-lg-10 ">
		        	<b>Address Proof :</b>
		        	</div>
		        	<div class="col-sm-2 col-md-2 col-lg-2">
		        		<?php echo generateAdminFileDownload($customerDetails['addressProof']); ?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-10 col-md-10 col-lg-10 ">
		        	<b>ID Proof :</b>
		        	</div>
		        	<div class="col-sm-2 col-md-2 col-lg-2">
		        		<?php echo generateAdminFileDownload($customerDetails['idProof']);?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-10 col-md-10 col-lg-10 ">
		        	<b>Additional File1 :</b>
		        	</div>
		        	<div class="col-sm-2 col-md-2 col-lg-2">
		        		<?php echo generateAdminFileDownload($customerDetails['additionalFile1']);?>
		        	</div>
		        	<div class="clearfix"></div>
		        	<div class="col-sm-10 col-md-10 col-lg-10 ">
		        	<b>Additional File2 :</b>
		        	</div>
		        	<div class="col-sm-2 col-md-2 col-lg-2">
		        		<?php echo generateAdminFileDownload($customerDetails['additionalFile2']);?>
		        	</div>
		        </div>
		        <div class="col-lg-4 col-md-4 padding-25px-0px border-left">
		        	<a <?php echo ($customerDetails['documentsFlag']) ? "disabled":"";?> href="<?php $customerNumber=$customerDetails['customerNumber']; echo admin_url("clients/approve/docs/$customerNumber"); ?>" class="btn tbn-transparent fa fa-check text-success"></a>
		        </div>
	        	<div class="clearfix"></div>
		        <div class="col-lg-12 col-md-12">
		        	<div class="col-sm-6 col-md-6 col-lg-6 ">
		        	<b>Terms Flag :</b>
		        	</div>
		        	<div class="col-sm-6 col-md-6 col-lg-6">
		        		<b><?php echo ($customerDetails['termsFlag'] == 1) ? "<span class='text-success'>Accepted</span>":"<span class='text-danger'>Unaccepted</span>";?></b>
		        	</div>
		        </div>

        	</div>


        </div>
        <?php if($customerDetails['companyID']!='0'){ ?>
        <div class="col-lg-2 col-sm-2 padding-0px">
        	<div class="clearfix"></div>
        	<b>Company Name :</b> <?php echo $companyAddressDetails['name'] ;?>
        	<div class="clearfix"></div>
        	<b>Company Address :</b> <?php echo $companyAddressDetails['addr'] ;?>
        	<div class="clearfix"></div>
        	<b>Company City :</b> <?php echo $companyAddressDetails['city'] ;?>
        	<div class="clearfix"></div>
        	<b>Company State :</b> <?php echo $companyAddressDetails['state'] ;?>
        	<div class="clearfix"></div>
        	<b>Company PIN :</b> <?php echo $companyAddressDetails['pin'] ;?>
        	<div class="clearfix"></div>
        	<b>Company office Phone :</b> <?php echo $companyAddressDetails['officePhone'] ;?>
        	<div class="clearfix"></div>
        	<b>Company TIN :</b> <?php echo $companyAddressDetails['tin'] ;?>
        	<div class="clearfix"></div>
        	<b>Company PAN :</b> <?php echo $companyAddressDetails['pan'] ;?>
        	<div class="clearfix"></div>
        	<b>Company CST :</b> <?php echo $companyAddressDetails['cst'] ;?>

        </div>
        <?php } ?>
        <div class="clearfix"></div>
        <hr>
        <div class="col-sm-4 col-md-4 col-lg-4">
        	<h3 class="text-center margin-top-10px" >PERSOANL ADDRESS</h3>
	        <form method="post" action="<?php echo admin_url('clients/addressAction');?>">
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Address :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressLine1' value='".((empty($address['HOME']['addressLine1']))?'':$address['HOME']['addressLine1'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">

	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressLine2' value='".((empty($address['HOME']['addressLine2']))?'':$address['HOME']['addressLine2'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>City :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressCity' value='".((empty($address['HOME']['addressCity']))?'':$address['HOME']['addressCity'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>State :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressState' value='".((empty($address['HOME']['addressState']))?'':$address['HOME']['addressState'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Pin Code :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressPin' value='".((empty($address['HOME']['addressPin']))?'':$address['HOME']['addressPin'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Land Mark :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressLandmark' value='".((empty($address['HOME']['addressLandmark']))?'':$address['HOME']['addressLandmark'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Shipping Cost</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<input type="hidden" name="addressID" value="<?php if(!empty($address['HOME']['customerId'])) { echo $address['HOME']['addressID']; } ?>">
        		<input type="hidden" name="type" value="HOME">
        		<input type="hidden" name="customerNumber" value="<?php echo $customerDetails['customerNumber'];?>">
  				<input type="text" name="shippingcost" class="height-20px" value="<?php if(!empty($address['HOME']['customerId'])) { echo $address['HOME']['shippingCost']; }?>">
	        </div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
			</div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
						<?php if(!empty($address['HOME']['customerId'])) { echo $addressStatusButtons[$address['HOME']['status']]; }?>
						<button value ="save" name="addrButton" class="btn btn-primary btn-xs" type="submit" >Save</button>
			</div>
	        </form>
    	</div>
    	<div class="col-sm-4 col-md-4 col-lg-4">
        	<h3 class="text-center margin-top-10px" >OFFICE ADDRESS</h3>
	        <form method="post" action="<?php echo admin_url('clients/addressAction');?>">
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Address :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressLine1' value='".((empty($address['OFFICE']['addressLine1']))?'':$address['OFFICE']['addressLine1'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">

	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressLine2' value='".((empty($address['OFFICE']['addressLine2']))?'':$address['OFFICE']['addressLine2'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>City :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressCity' value='".((empty($address['OFFICE']['addressCity']))?'':$address['OFFICE']['addressCity'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>State :</b>
	        </div>	
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressState' value='".((empty($address['OFFICE']['addressState']))?'':$address['OFFICE']['addressState'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Pin Code :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					echo "<input type='text' name='addressPin' value='".((empty($address['OFFICE']['addressPin']))?'':$address['OFFICE']['addressPin'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Land Mark :</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<?php 
					 echo "<input type='text' name='addressLandmark' value='".((empty($address['OFFICE']['addressLandmark']))?'':$address['OFFICE']['addressLandmark'])."'>";
				?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
	        	<b>Shipping Cost</b>
	        </div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
	        	<input type="hidden" name="type" value="OFFICE">
        		<input type="hidden" name="customerNumber" value="<?php echo $customerDetails['customerNumber'];?>">
  				<input type="hidden" name="addressID" value="<?php if(!empty($address['OFFICE']['customerId'])) { echo $address['OFFICE']['addressID']; }?>">
        		<input type-"text" name="shippingcost" class="height-20px" value="<?php if(!empty($address['OFFICE']['customerId'])) { echo $address['OFFICE']['shippingCost']; }?>">
        		
	        </div>
	        <div class="clearfix"></div>
	        <div class="col-sm-4 col-md-4 col-lg-4 text-right">
			</div>
	        <div class="col-sm-8 col-md-8 col-lg-8">
						<?php if(!empty($address['OFFICE']['customerId'])) { echo $addressStatusButtons[$address['OFFICE']['status']]; }?>
						<button value ="save" name="addrButton" class="btn btn-primary btn-xs" type="submit" >Save</button>
			</div>
	        </form>
    	</div>
    	<div class="col-sm-4 col-md-4 col-lg-4">
    		<div class="clearfix"></div>
    		<br>
    		<form class="form-horizontal" role="form" method = "post" action="<?php echo admin_url('clients/changeStatus');?>">
  				<input type="hidden" name="customerNumber" value="<?php echo $customerDetails['customerNumber'];?>">
  				<div class="form-group">
    				<label class="control-label col-sm-5 col-md-5 col-lg-5 ">Comments :</label>
    				<div class="col-sm-7 col-md-7 col-lg-7">
    					<textarea class="form-control" name="comments"><?php echo $customerDetails['comments'];?></textarea>
    				</div>
    			</div>
    			<div class="form-group">
    				<label class="col-sm-5 col-md-5 col-lg-5 control-label">Status :</label>
    				<div class="col-sm-7 col-md-7 col-lg-7">
    					<b><?php echo $customerStatusText[$customerDetails['status']] ;?></b>
    				</div>
    			</div>
    			<div class="from-group">
    				<div class="col-sm-offset-5 col-lg-offset-5 col-sm-7 col-lg-7">
	    				<?php if($customerDetails['status'] != 1 && $customerDetails['status'] != 2 ) {?><button type="submit" class="btn btn-success" name = "statusBtn" value="1"> Approve </button><?php } ?>
	    				<?php if($customerDetails['status'] != 2) {?><button type="submit" class="btn btn-danger" name = "statusBtn" value="2"> Block </button><?php } ?>
	    				<?php if($customerDetails['status'] == 2) {?><button type="submit" class="btn btn-danger" name = "statusBtn" value="1"> Unblock </button><?php } ?>
	    			</div>
    			</div>
    		</form>
    	</div>
    </div>
</div>
