<?php $customerStatus = array('<span class = "text-danger">Unapproved</span>','Approved','<span class="text-danger">Blocked</span>','Change Request');?>



<div class="clearfix"></div>

<br>

<div class="col-sm-2 col-md-2"></div>

<div class="col-sm-8 col-md-8">

  <div class="row">

    <div class="col-md-12">

    <?php

      $breadcrumbText = '';

      foreach ($breadcrumb as $breadcrumbRow){

        $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];

        $breadcrumbText .= " > ";

      }

      echo rtrim($breadcrumbText, " > ");

    ?>



    <div class="full-width-tabs border-5px-solid-C1272D height-100">

          <ul class="tabs nav  nav-tabs margin-bottom-15px background-c1272d width-1005 border-bottom-0px">

          <?php if($terms==0){ ?>

            <li class="take-all-space-you-can disabled width-25" ><a class="padding-8px-0px textcenter color-fff"   >MYACCOUNT</a></li>

            <li class="take-all-space-you-can disabled width-25" ><a class="padding-8px-0px textcenter color-fff" >POINTS</a></li>

            <li class="take-all-space-you-can disabled width-25" ><a class="padding-8px-0px textcenter color-fff" >ORDERS</a></li>

            <li class="active take-all-space-you-can width-24-85" ><a class="padding-8px-0px textcenter " href="#terms" data-toggle="tab">T&C</a></li>



            <?php } else { ?>

            <li class="active take-all-space-you-can width-20"><a class="padding-8px-0px textcenter " href="#myaccount"  data-toggle="tab">MYACCOUNT</a></li>

            <li class="take-all-space-you-can width-20" ><a class="padding-8px-0px textcenter " href="#points" data-toggle="tab">POINTS</a></li>

            <li class="take-all-space-you-can width-20" ><a class="padding-8px-0px textcenter " href="#orders" data-toggle="tab">ORDERS</a></li>

            <li class="take-all-space-you-can width-20" ><a class="padding-8px-0px textcenter " href="#payment" data-toggle="tab">PAYMENT</a></li>

            <li class="take-all-space-you-can width-19-85" ><a class="padding-8px-0px textcenter " href="#terms" data-toggle="tab">T&C</a></li>



            <?php } ?>

          </ul>



          <div id="myTabContent" class="tab-content padding-10px" >

            <?php if($terms==0){ ?>

              <div class="tab-pane fade " id="myaccount">

                <?php } else { ?>

                <div class="tab-pane active fade in" id="myaccount">

                <?php } ?>

                <div class="col-sm-2 col-md-2 col-lg-2 ">

                    <?php  $clientPhoto=site_url('uploads/'.$customerDetails['customerImagePath']);

                    

        

                     // var_dump(basename($clientPhoto["path"]));

                     // var_dump(file_exists($customerDetails['customerImagePath']));

                      // var_dump($this->config->config['upload_path'].$customerDetails['customerImagePath']);

                       //var_dump(file_exists($this->config->config['upload_path'].$customerDetails['customerImagePath']));

                      //var_dump($clientPhoto["path"]);

                      // var_dump(file_exists($clientPhoto["path"]));

                     // var_dump(basename($clientPhoto["path"]));

                     // var_dump(file_exists(basename($clientPhoto["path"])));

                      //var_dump(basename($clientPhoto["path"]));

                      //var_dump(empty($customerDetails['customerImagePath']));

                      //die();

                    //  if(file_exists($clientPhoto))

                      if(!empty($customerDetails['customerImagePath'])) { ?>

                        <img src="<?php echo site_url('uploads/'.$customerDetails['customerImagePath']); ?>" class="img-responsive">

                    <?php  } else { ?>

                        <img src="<?php echo site_url('images/custImage.png'); ?>" class="img-responsive">

                    <?php } ?>

                  </div>

                <div class="col-sm-5 col-md-5 col-lg-5 ">

                  <div class="custAccount">

                    <div id="custName" class="text-uppercase"><?php echo $customerDetails['firstName']; ?>&nbsp;<?php echo $customerDetails['lastName']; ?> ( <b class="font-size-14px"><?php echo $customerStatus[$customerDetails['status']]; ?></b> )</div>

					<div id="memberType">Customer  Number:<span class="font-weight-bold"><?php echo $customerDetails['customerNumber']; ?></span></div>

                    <div id="memberType">Membership Type:<span class="font-weight-bold">Basic</span></div>

				    <div id="changePass" class="text-color">Password:<span class="font-weight-bold"><a class="test-color" href="<?php echo site_url('myaccount/changePass'); ?>">Change Password</a></span></div>

				</div>



                </div>

                <div class="col-sm-5 col-md-5 col-lg-5">

                  <div class="float-right text-right font-size-15px font-family-helvetica;">

                    Member Since : <?php echo $customerDetails['registeredDate']; ?>

                  </div>

                  <div class="clearfix"></div>

                  <br>

                  <div class="text-color float-right" id="pancarddetails">Pan Number: <?PHP if(!empty($customerDetails['pancardNumber'])) { ?> <span class="font-weight-bold"><?php echo $customerDetails['pancardNumber']; }?> </span> <?php if($customerDetails['status']==0) { ?><span id="editpan" class="cursor-pointer"><i class="fa fa-pencil-square-o" title="EDIT"></i></span> <?php } ?><br></div>

                  <?php if($customerDetails['status']==0) { ?>

                    <div id="editpandetails" class="hide">

                        <form class="form-horizontal" role="form" action="myaccount/updatePanInfo" method="post">

                           <div class="form-group">

                            <label class="control-label col-sm-offset-1 col-sm-5 accountAddressDiv padding-5px-0px-5px-5px" > Pan Number:</label>

                              <div class="col-sm-6">

                                 <input type="text" name="pannumber" id="pannumber" class="form-control input-xs" value="<?php echo $customerDetails['pancardNumber']; ?> " Placeholder="Enter Pan Number">

                              </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">

                              <div class="col-sm-offset-7 col-sm-5">

                                <button class="btn btn-primary btn-xs" type="submit"> Save </button>

                                <a onclick="showHide('pancarddetails','editpandetails')" class="btn btn-warning btn-xs " type="submit"> Cancel </a>

                              </div>

                            </div>

                            <div class="clearfix"></div>

                        </form>

                    </div>

                  <?php } ?>

                  <div class="clearfix"></div>

                  <div class="text-color float-right" id="dobdetails">DOB: <?PHP if(!empty($customerDetails['DOB'])) { ?> <span class="font-weight-bold"><?php echo $customerDetails['DOB']; }?> </span> <?php if($customerDetails['status']==0) { ?> <span id="editdob" class="cursor-pointer"><i class="fa fa-pencil-square-o" title="EDIT"></i></span> <?php } ?> </div>

                  <?php if($customerDetails['status']==0) { ?>

                    <div id="editdobdetails" class="hide">

                        <form class="form-horizontal" role="form" action="myaccount/updateDOBInfo" method="post">



                            <div class="form-group">

                            <label class="control-label col-sm-offset-1 col-sm-5 accountAddressDiv padding-25px-0px-5px-5px" > Date of Birth:</label>

                              <div class="col-sm-6">

                                 <input type="text" name="dob" id="dob" class="form-control " value="<?php echo $customerDetails['DOB']; ?> " Placeholder="DOB">

                              </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">

                              <div class="col-sm-offset-7 col-sm-5">

                                <button class="btn btn-primary btn-xs" type="submit"> Save </button>

                                <a onclick="showHide('dobdetails','editdobdetails')" class="btn btn-warning btn-xs" type="submit"> Cancel </a>

                              </div>

                            </div>

                            <div class="clearfix"></div>

                        </form>

                    </div>

                  <?php } ?>

                </div>

                <div class="clearfix"></div>

				<?php if($customerDetails['status']==0) { ?> 

                <div class="col-md-10 col-md-offset-2">

					<p><b><span class='text-danger'>Membership Fees of Rs 500 (non refundable) has to be paid<span></b></p>

				</div>

				<div class="clearfix"></div>

				<?php } ?>

                <hr>

                <div class="edit_Title margin-top:15px">Personal Address</div>

                <div id="displaypersonaldetails">

                  <div class="col-sm-6 col-md-6">



                    <?php

                    if(!empty($homeAddress['addressLine1']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Address </b> : </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressLine1']."</div>"; }

                    if(!empty($homeAddress['addressLine2']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b> </b>  </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressLine2']."</div>"; }

                    if(!empty($homeAddress['addressCity']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>City </b> : </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressCity']."</div>"; }

                    if(!empty($homeAddress['addressState']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>State </b> : </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressState']."</div>"; }

                    if(!empty($homeAddress['addressPin']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Pin </b> : </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressPin']."</div>"; }

                    if(!empty($homeAddress['addressLandmark']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Land Mark </b> : </div><div class='col-sm-6 col-md-6 '>".$homeAddress['addressLandmark']."</div>"; }

                    if($customerDetails['status'] == 3  && $homeAddress['status'] == 0)

                      { echo "<div class='col-sm-12 col-md-12 text-danger'>(address is yet to be approved)</div>"; }

                    ?>

                  </div>

                  <div class="col-sm-offset-4 col-sm-2 text-color cursor-pointer" id="edit" ><b><i class="fa fa-pencil-square-o"></i> <?php echo ($customerDetails['status']==0) ? 'EDIT': 'CHANGE'; ?>  </b></div>

                  <div class="clearfix"></div>



                </div>

                <div id="editpersondetails" class="hide">

                  <form class="form-horizontal" role="form" <?php if(!empty($homeAddress['customerId'])) { ?> action="myaccount/updatePersonalAddress" <?php  }else{ ?> action="myaccount/insertPersonalAddress" <?php } ?> method="post">

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Address  :</label>

                      <div class="col-sm-4">

                        <input type="text" name="paddress" id="paddress" value="<?php if(!empty($homeAddress['customerId'])) { echo $homeAddress['addressLine1']; }?>" class="form-control"  Placeholder="Enter Address line1"  required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > </label>

                      <div class="col-sm-4">

                        <input type="text" name="paddress1" id="paddress1" value="<?php if(!empty($homeAddress['customerId'])) {  echo $homeAddress['addressLine2']; }?>" class="form-control" Placeholder="Enter Address line2" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > City  :</label>

                        <div class="col-sm-4">

                          <input type="text" name="pcity" id="pcity" value="<?php if(!empty($homeAddress['customerId'])) {   echo $homeAddress['addressCity']; }?>" class="form-control" Placeholder="Enter City" required><span class="text-color">*</span>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > State  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="pstate" id="pstate" value="<?php if(!empty($homeAddress['customerId'])) {   echo $homeAddress['addressState']; }?>" class="form-control" Placeholder="Enter State" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Pin Code  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="ppincode" id="ppincode" value="<?php if(!empty($homeAddress['customerId'])) { echo $homeAddress['addressPin']; }?>" class="form-control" Placeholder="Enter Pin Code" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Land Mark  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="plandmark" id="plandmark" value="<?php if(!empty($homeAddress['customerId'])) {   echo $homeAddress['addressLandmark']; }?>" class="form-control" Placeholder="Enter Land Mark" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <div class="col-sm-offset-3 col-sm-4">

                        <button class="btn btn-primary" type="submit"> Save </button>

                        <a onclick ="showHide('displaypersonaldetails','editpersondetails')" class="btn btn-warning " type="submit"> Cancel </a>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                  </form>

                </div>



                <div class="clearfix"></div>

                <hr>

                <div class="edit_Title margin-top-15px" >Contact Numbers</div>

                <div id="displaycontactdetails">

                  <div class="col-sm-6 col-md-6">



                    <?php

                    if(!empty($customerDetails['mobileNumber']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Mobile Number </b> : </div><div class='col-sm-6 col-md-6 '>".$customerDetails['mobileNumber']."</div>"; }

                    if(!empty($customerDetails['landlineNumber']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Landline Number </b> : </div><div class='col-sm-6 col-md-6 '>".$customerDetails['landlineNumber']."</div>"; }

                    if(!empty($customerDetails['officeNumber']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Office Number </b> : </div><div class='col-sm-6 col-md-6 '>".$customerDetails['officeNumber']."</div>"; }

                    if($customerDetails['status'] == 3  && $customerDetails['contactNumberFlag'] == 1)

                      { echo "<div class='col-sm-6 col-md-6 text-danger text-right'>(yet to be approved)</div>"; }

                        ?>

                  </div>

                  <div class="col-sm-offset-4 col-sm-2 text-color cursor-pointer" id="editcontact" ><b><i class="fa fa-pencil-square-o"></i> <?php echo ($customerDetails['status']==0) ? 'EDIT': 'CHANGE'; ?>  </b></div>

                  <div class="clearfix"></div>



                </div>

                <div id="editcontactdetails" class="hide">

                  <form class="form-horizontal" role="form" action="myaccount/updateContactInfo" method="post">

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Mobile Number  :</label>

                      <div class="col-sm-4">

                        <input type="text" name="mnumber" id="mnumber" value="<?php echo $customerDetails['mobileNumber']; ?>" class="form-control" Placeholder="Enter Mobile Number" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Landline Number  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="lnumber" id="lnumber" value="<?php echo $customerDetails['landlineNumber']; ?>" class="form-control" Placeholder="Enter Landline Number">

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Office Number  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="onumber" id="onumber" value="<?php echo $customerDetails['officeNumber']; ?>" class="form-control" Placeholder="Enter Office Number">

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <div class="col-sm-offset-3 col-sm-4">

                        <button class="btn btn-primary" type="submit"> Save </button>

                        <a onclick ="showHide('displaycontactdetails','editcontactdetails')" class="btn btn-warning " type="submit"> Cancel </a>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                  </form>

                </div>



                <div class="clearfix"></div>

                <hr>

                <div class="edit_Title margin-top-15px">Office Address</div>

                <div id="displayofficedetails">

                  <div class="col-sm-6 col-md-6">

                    <?php

                    if(!empty($officeAddress['addressLine1']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Address </b> : </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressLine1']."</div>"; }

                    if(!empty($officeAddress['addressLine2']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b> </b>  </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressLine2']."</div>"; }

                    if(!empty($officeAddress['addressCity']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>City </b> : </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressCity']."</div>"; }

                    if(!empty($officeAddress['addressState']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>State </b> : </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressState']."</div>"; }

                    if(!empty($officeAddress['addressPin']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Pin </b> : </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressPin']."</div>"; }

                    if(!empty($officeAddress['addressLandmark']))

                      { echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Land Mark </b> : </div><div class='col-sm-6 col-md-6 '>".$officeAddress['addressLandmark']."</div>"; }

                    if($customerDetails['status'] == 3  && $officeAddress['status'] == 0)

                      { echo "<div class='col-sm-6 col-md-6 text-danger text-right'>(address is yet to be approved)</div>"; }

                    ?>



                  </div>

                  <div class="col-sm-offset-4 col-sm-2 text-color cursor-pointer" id="editoffice"><b><i class="fa fa-pencil-square-o"></i> <?php echo ($customerDetails['status']==0) ? 'EDIT': 'CHANGE'; ?>  </b></div>

                </div>

                <div id="editofficedetails" class="hide">

                  <form class="form-horizontal" role="form" <?php if(!empty($officeAddress['customerId'])) { ?> action="myaccount/updateOfficeAddress" <?php  }else{ ?> action="myaccount/insertOfficeAddress" <?php } ?> method="post">

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Address  :</label>

                      <div class="col-sm-4">

                        <input type="text" name="oaddress" id="oaddress" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressLine1']; } ?>" class="form-control" Placeholder="Enter Address line1"  required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="form-group">

                    <label class="control-label col-sm-3 accountAddressDiv" > </label>

                      <div class="col-sm-4">

                         <input type="text" name="oaddress1" id="oaddress1" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressLine2']; }?>" class="form-control" Placeholder="Enter Address line2" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > City  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="ocity" id="ocity" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressCity']; }?>" class="form-control" Placeholder="Enter City" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > State  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="ostate" id="ostate" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressState']; } ?>" class="form-control" Placeholder="Enter State" required><span class="text-color">*</span>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Pin Code  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="opincode" id="opincode" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressPin']; }?>" class="form-control" Placeholder="Enter Pin Code" required><span class="text-color">*</span>

                      </div>



                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <label class="control-label col-sm-3 accountAddressDiv" > Land Mark  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="olandmark" id="olandmark" value="<?php if(!empty($officeAddress['customerId'])) { echo $officeAddress['addressLandmark']; }?>" class="form-control" Placeholder="Enter Land Mark" required><span class="text-color">*</span>

                      </div>



                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <div class="col-sm-offset-3 col-sm-4">

                        <button class="btn btn-primary" type="submit"> Save </button>

                        <a onclick ="showHide('displayofficedetails','editofficedetails')" class="btn btn-warning " type="submit"> Cancel </a>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                  </form>

                </div>

                <div class="clearfix"></div>

                <hr>

                <div class="edit_Title margin-top-15px">Company Information</div>

                <div id="displaycompinfodetails">

                  <div class="col-sm-6 col-md-6">

                    <?php

                    if(!empty($customerDetails['companyPerson']))

                      { $registeredType='';

                        if($customerDetails['companyPerson']=='C')

                          {

                            $registeredType="Company";

                          }

                          else

                          {

                            $registeredType="Individual";

                          }

                        echo "<div class='col-sm-6 col-md-6 text-color text-right'><b>Registered as a </b> : </div><div class='col-sm-6 col-md-6 '>".$registeredType."</div>";

                      }

                    if($customerDetails['companyPerson']=='C')

                      { ?>

                         <div class='col-sm-6 col-md-6 text-color text-right'><b>Company Name</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['name'])){ echo $companyAddress['name']; }?> </div> 

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company Address</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['addr'])) {echo $companyAddress['addr']; }?> </div> 

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company City</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['city'])) {echo $companyAddress['city']; }?> </div> 

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company State</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['state'])) {echo $companyAddress['state']; }?> </div> 

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company Off. Number</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['officePhone'])) { echo $companyAddress['officePhone']; }?> </div>

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company Pan</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['pan'])) {echo $companyAddress['pan']; }?> </div> 

                        <div class="clearfix"></div>

                        <div class='col-sm-6 col-md-6 text-color text-right'><b>Company GST</b> : </div>

                        <div class='col-sm-6 col-md-6 '> <?php if(!empty($companyAddress['cst'])) {echo $companyAddress['cst']; }?> </div>  

                      <?php } ?>

                  </div>

                  <?php if($customerDetails['companyPerson']=='C')

                      { ?>

                  <div class="col-sm-offset-4 col-sm-2 text-color cursor-pointer" id="editcompinfo" ><b><i class="fa fa-pencil-square-o"></i> <?php echo ($customerDetails['status']==0) ? 'EDIT': 'CHANGE'; ?>  </b></div>

                  <?php } ?>

                </div>

                <div id="editcompinfodetails" class="hide">

                  <form class="form-horizontal" role="form" action="myaccount/updateCompanyInfo" method="POST">

                    <div class="clearfix"></div>

                    <INPUT type="hidden" name="companyChoice" value="C">

                      <input type="hidden" name="companyAddressID" value="<?php if(!empty($companyAddress['companyID'])){ echo $companyAddress['companyID']; }?>">

                    

                    <div class="form-group" id="companyinput">

                    <label class="control-label col-sm-3 accountAddressDiv" > Company Name  :</label>

                      <div class="col-sm-4">

                         <input type="text" name="cname" id="cname" value="<?php if(!empty($companyAddress['name'])){ echo $companyAddress['name']; }?>" class="form-control" Placeholder="Enter Company Name" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company Address  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="caddressLine1" id="caddressLine1" value="<?php if(!empty($companyAddress['addr'])) {echo $companyAddress['addr']; }?>" class="form-control" Placeholder="Enter Company Address Line1" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company City  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="ccity" id="ccity" value="<?php if(!empty($companyAddress['city'])) {echo $companyAddress['city']; }?>" class="form-control" Placeholder="Enter Company City" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company State  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="cstate" id="cstate" value="<?php if(!empty($companyAddress['state'])) {echo $companyAddress['state']; }?>" class="form-control" Placeholder="Enter Company State" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company Office Number  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="coffice" id="coffice" value="<?php if(!empty($companyAddress['officePhone'])) { echo $companyAddress['officePhone']; }?>" class="form-control" Placeholder="Enter Office Number" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company Pan Number  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="cpan" id="cpan" value="<?php if(!empty($companyAddress['pan'])) {echo $companyAddress['pan']; }?>" class="form-control" Placeholder="Enter Company Pan Number" requiered>

                      </div>

                      <div class="clearfix"></div>

                      <label class="control-label col-sm-3 accountAddressDiv margin-top-5px" > Company GST Number  :</label>

                      <div class="col-sm-4 margin-top-5px">

                         <input type="text" name="ccst" id="ccst" value="<?php if(!empty($companyAddress['cst'])) {echo $companyAddress['cst']; }?>" class="form-control" Placeholder="Enter Company CST Number" >

                      </div>

                      



                    </div>

                    

                    

                    <div class="clearfix"></div>

                    <div class="form-group">

                      <div class="col-sm-offset-3 col-sm-4">

                        <button class="btn btn-primary" type="submit"> Save </button>

                        <a onclick ="showHide('displaycompinfodetails','editcompinfodetails')" class="btn btn-warning " type="submit"> Cancel </a>

                      </div>

                    </div>

                    <div class="clearfix"></div>

                  </form>

                </div>

                <div class="clearfix"></div>



                <hr>

                <div class="edit_Title margin-top-15px" >Upload Files</div>

                <form method="post" class="form-horizontal" role="form" action="<?php echo site_url('myaccount/fileUploads');?>" enctype="multipart/form-data">



                <label class="control-label col-sm-3 accountAddressDiv" > Address Proof  :</label>

                  <div class="col-sm-9">

                     <input type="file" name="uploads[addressProof]" id="addressProof" >

                     <label><?php echo generateFileDownload($customerDetails['addressProof']);?></label>

                  </div>





                <label class="control-label col-sm-3 accountAddressDiv"> ID Proof  :</label>

                  <div class="col-sm-9">

                     <input type="file" name="uploads[idProof]" id="idProof" >

                     <label><?php echo generateFileDownload($customerDetails['idProof']);?></label>

                  </div>





                <label class="control-label col-sm-3 accountAddressDiv"> Recent Photograph  :</label>

                  <div class="col-sm-9">

                     <input type="file" name="uploads[recentPhoto]" id="recentPhoto" >

                     <label><?php echo generateFileDownload($customerDetails['customerImagePath']);?></label>

                  </div>





                <label class="control-label col-sm-3 accountAddressDiv "> Additional File1  :</label>

                  <div class="col-sm-9 ">

                     <input type="file" name="uploads[additionalFile1]" id="additionalFile1" >

                     <label><?php echo generateFileDownload($customerDetails['additionalFile1']);?></label>

                  </div>





                <label class="control-label col-sm-3 accountAddressDiv"> Additional File2  :</label>

                  <div class="col-sm-9">

                     <input type="file" name="uploads[additionalFile2]" id="additionalFile2" >

                     <label><?php echo generateFileDownload($customerDetails['additionalFile2']);?></label>

                  </div>



                <div class="form-group">

                <label class="control-label col-sm-3"> </label>

                  <div class="col-sm-9">

                     <button type="submit" class="btn btn-danger"> UPLOAD </button>

                  </div>

                </div>

              </form>

            </div>



            <div class="tab-pane fade" id="points">

                <div class="row row-xs-height">

					<div class="col-md-4 col-xs-height" style="height:350px;">

						<div class="accountBorderdesign margin-top-64px">

							<center>

							<?php

								$earnedCost = (array_key_exists('EARNED', $points_vallet_summary)? $points_vallet_summary['EARNED']: "0");

								$spentCost = (array_key_exists('SPENT', $points_vallet_summary)? $points_vallet_summary['SPENT']: "0");

							?>

							<div class="orderHeading line-height-150" style="font-size: x-large;">Order Points</div>

							<div class="hrstyle" style="0 10px 10px 10px;width: auto;"></div>

							<?php if($promocode == null) { ?>

								<?php if(($earnedCost - $spentCost) >= 250 ) { ?>

									<div class="margin-left-10px font-size-14px font-weight-bold">

										<button onclick="generateDiscount()" class="btn btn-danger" style="margin: 10px 0px;padding: 10px;">

											Generate Discount Voucher (Rs 250)

										</button>

									</div>

								<?php } else {?>

									<div class="margin-left-10px font-size-16px">Available Voucher</div>

									<div class="font-weight-bold margin-left-10px font-size-16px">no Voucher available</div>

									<div class="margin-left-10px font-size-11px text-danger">need 250 points to generate discount voucher</div>

								<?php } ?>

							<?php }else{ ?>

								<div class="margin-left-10px font-size-16px">Available Voucher</div>

								<div class="font-weight-bold margin-left-10px font-size-16px"><?php echo $promocode; ?></div>

								<!--<div class="margin-left-10px font-size-11px text-danger">need 250 points to generate discount voucher</div>-->

							<?php } ?>

							<div class="hrstyle" style="margin: 0 10px 10px 10px;width: auto;"></div>

							<div class="margin-left-10px font-size-16px">Points earned till dates</div>

							<div class="font-weight-bold margin-left-10px font-size-16px">Rs.<?php echo $earnedCost; ?></div>

							<div class="hrstyle" style="margin: 0 10px 10px 10px;width: auto;"></div>

							<div class="margin-left-10px line-height-150 font-size-16px">Points used till dates</div>

							<div class="font-weight-bold margin-left-10px font-size-16px">Rs.<?php echo $spentCost; ?></div>

							</center>

						</div>

					</div>

					<div class="col-md-4 col-xs-height" style="border: 1px solid red;border-radius: 5px;height: 350px;display: block;overflow-x: auto;">

						<table class="table">

							<caption style="font-size: x-large;" class="orderHeading line-height-150"><center><b>Points Earned</b></center></caption>

							<?php if(array_key_exists('EARNED', $points_vallet_summary)) { ?>

							<tbody>

								<tr>

									<th>OrderID</th>

									<th>Points</th>

								</tr>

								<?php foreach($points_vallet_details['EARNED'] as $row) { ?>

									<tr>

										<td><?php echo ($row['orderID'] == -1)?"Consolidated":$row['orderID']; ?></td>

										<td><?php echo $row['points']; ?></td>

									</tr>

									<?php } ?>

							</tbody>

							<?php } else{ ?>

								<tr><th colspan='2'><center>no points spent</center></th></tr>

							<?php } ?>

						</table>

					</div>

					<div class="col-md-4 col-xs-height">

						<div style="border: 1px solid red;border-radius: 5px;height: 350px;display: block;overflow-x: auto;">

						<table class="table">

							<caption style="font-size: x-large;" class="orderHeading line-height-150"><center><b>Points Spent</b></center></caption>

							<?php if(array_key_exists('SPENT', $points_vallet_summary)) { ?>

							<tbody>

								<tr>

									<th>Voucher</th>

									<th>Points</th>

								</tr>

								<?php foreach($points_vallet_details['SPENT'] as $row) { ?>

								<tr>

									<td><?php echo $row['promocode']; ?></td>

									<td><?php echo $row['points']; ?></td>

								</tr>

								<?php } ?>

								

							</tbody>

							<?php } else{ ?>

								<tr><th colspan='2'><center>no points spent</center></th></tr>

							<?php } ?>

						</table>

						</div>

					</div>

				</div>

			</div>

			<div class="tab-pane fade" id="orders">

                <div class="table-responsive">

                  <h4 class="text-center text-danger text-uppercase"></h4>

                  <table class="table table-bordered table-responsive table-striped datatable">

                    <thead>

                      <tr>

                        <th>Order ID</th>

                        <th>Sub Total</th>

                        <th>Discount </th>

                        <th>Total Amount</th>

                        <th>Status</th>

                        <th></th>

                      </tr>

                    </thead>

                    <tbody>

                       <?php

                          $i=0;

                          foreach ($orderDetails as $row) {

                            $i++;

                        ?>

                        <tr class="mainrow mainrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">

                          <td><?php echo $row['orderNumber']; ?></td>

                          <td><i class="fa fa-inr"></i> <?php echo $row['orderSubtotal']; ?></td>

                          <td><i class="fa fa-inr"></i> <?php echo $row['orderDiscountAmount']; ?></td>

                          <td><i class="fa fa-inr"></i> <?php echo $row['orderTotal']; ?></td>

                          <td><?php echo $row['orderStatusDisplayName']; ?></td>

                          <td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $row['orderNumber']; ?>"> View</a></td>

                        </tr>

                        <tr class ="childrow childrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">

                          <td colspan="6" >

                            <table class="table">

                              <thead class="font-size-12px">

                                <tr>

                                  <th>Item Name</th>

                                  <th>Collect Date</th>

                                  <th>Start Date</th>

                                  <th>End Date</th>

                                  <th>Reurn Date</th>

                                  <th>Total Rent Amount</th>

                                  <th>Status</th></tr>

                              </thead>

                              <tbody class="font-size-12px">



                                 <?php foreach ($row['items'] as $items) { ?>

                                 <tr>

                                  <td><?php echo $items['itemName']; ?></td>

                                  <td><?php echo dateFromMysqlDate($items['collectDate']); ?></td>

                                  <td><?php echo dateFromMysqlDate($items['startDate']); ?></td>

                                  <td><?php echo dateFromMysqlDate($items['endDate']); ?></td>

                                  <td><?php echo dateFromMysqlDate($items['returnDate']); ?></td>

                                  <td><i class="fa fa-inr"></i> <?php echo $items['rentalTotalPrice']; ?></td>

                                  <td><?php echo $items['orderStatusDisplayName']; ?></td>



                                </tr>

                                <?php } ?>

                              </tbody>

                            </table>

                            <table>

                              <thead>

                                <tr>

                                <th class="font-size-14px">Delivery Address</td>

                                <th class="font-size-14px">Pickup Address</td>

                                </tr>

                              </thead>

                              <tbody>

                                <tr>

                                  <td class="font-size-12px"><?php echo $row['deleveryAddressTYPE']; ?> </td>

                                  <td class="font-size-12px"><?php echo $row['pickupAddressTYPE']; ?> </td>

                                </tr>

                              </tbody>

                            </table>

                          </td>

                        </tr>

                      <?php } ?>

                    </tbody>

                  </table>

                </div>

            </div>

			<div class="tab-pane fade" id="payment">

                <div class="table-responsive">

                  <div id="paymentDiv">

                    <h4 class="text-center text-danger text-uppercase"></h4>



                    <table class="table table-bordered table-responsive table-striped datatable paymentTable">

                      <thead>

                        <tr>

                          <th>OrderNumbers</th>

                          <th>Total Order Amount</th>

                          <th>Paid Amount</th>

                          <th>Pending</th>

                          <th><button type="button" onclick="showPaymentForm(-1);" class="btn btn-primary">New Payment</button></th>

                        </tr>

                      </thead>

                      <tbody>

                         <?php

                            foreach ($paymentDetails as $row) {

                              $i++;

                          ?>

                          <tr class="mainrow mainrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">

                            <td><?php echo $row['orderNumbers']; ?></td>

                            <td><i class="fa fa-inr"></i> <?php echo $row['total']; ?></td>

                            <td><i class="fa fa-inr"></i> <?php echo $row['paid']; ?></td>

                            <td><i class="fa fa-inr"></i> <?php echo $row['pending']; ?></td>

                            <td><a class="btn btn-primary btn-xs"> View</a></td>

                          </tr>

                          <tr class ="childrow childrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">

                            <td colspan="6" >

                              <?php if(array_key_exists('details', $row)){ ?>

                              <table class="table">

                                <thead class="font-size-12px">

                                  <tr>

                                    <th>PaymentType</th>

                                    <th>Bank</th>

                                    <th>Branch</th>

                                    <th>Reference #</th>

                                    <th>Amount</th>

                                    <th>Date</th>

                                    <th>Status</th>

                                  </tr>

                                </thead>

                                <tbody class="font-size-12px">

                                   <?php foreach ($row['details'] as $items) { ?>

                                   <tr>

                                    <td><?php echo $items['typeDisplayName']; ?></td>

                                    <td><?php echo $items['bankName']; ?></td>

                                    <td><?php echo $items['branch']; ?></td>

                                    <td><?php echo $items['referenceNumber']; ?></td>

                                    <td><i class="fa fa-inr"></i><?php echo $items['amount']; ?></td>

                                    <td> <?php echo dateFromMysqlDate($items['date']); ?></td>

                                    <td><?php echo ($items['status']) ? 'Approved' : 'UnApproved'; ?></td>



                                  </tr>

                                  <?php } ?>

                                </tbody>

                              </table>

                              <?php } else {?>

                                No Payment Received

                              <?php } ?>



                            </td>

                          </tr>

                        <?php } ?>

                      </tbody>

                    </table>

                  </div>

                  <div id="paymentForm" style="min-height: 500px;" class="hide">

                    <form class="form-horizontal" role="form"  action="<?php echo site_url('myaccount/addPayment');?>" method="post">

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="orderSelect">OrderNumbers:</label>

                        <div class="col-sm-8">

                          <select required name="paymentOrderID" id="orderSelect" class="form-control" >

                            <?php echo $orderNumbersOption ;?>

                          </select>

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="paymentType">Payment Type:</label>

                        <div class="col-sm-8">

                          <select required name="paymentTypeID" id="paymentType" class="form-control" >

                            <?php echo $paymentOptions;?>

                          </select>

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="bankName">Bank Name:</label>

                        <div class="col-sm-8">

                          <input required type="text" class="form-control" id="bankName" name ="bankName" placeholder="Bank Name">

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="branchName">Branch Name:</label>

                        <div class="col-sm-8">

                          <input type="text" required class="form-control" id="branchName" name ="branchName" placeholder="Branch Name">

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="refNum">Reference #:</label>

                        <div class="col-sm-8">

                          <input required type="text" class="form-control" id="refNum" name ="refNum" placeholder="">

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="amount">Amount:</label>

                        <div class="col-sm-8">

                          <input required type="text" class="form-control" id="amount" name ="amount" placeholder="Amount Paid">

                        </div>

                      </div>

                      <div class="form-group">

                        <label class="control-label col-sm-4" for="date">Date:</label>

                        <div class="col-sm-8">

                          <input required type="text" class="form-control" id="date" name ="date">

                        </div>

                      </div>

                      <div class="form-group">

                        <div class="col-sm-offset-4 col-sm-2">

                          <button type="submit" class="btn btn-success">Submit</button>

                        </div>

                        <div class="col-sm-2">

                          <button type="reset" class="btn btn-danger">Reset</button>

                        </div>

                        <div class="col-sm-2">

                          <button type="button" onclick="cloasForm();" class="btn btn-info">Close</button>

                        </div>

                      </div>

                    </form>

                  </div>

                </div>

              </div>



              <?php if($terms==0){ ?>

              <div class="tab-pane active fade  in" id="terms">

                <?php } else { ?>

                <div class="tab-pane  fade  " id="terms">

                <?php } ?>



                  <h3 class="text-color"><strong>Terms and Conditions</strong></h3>



                                <p class="text-color"><b> Equipment</b></p>



                                <p class="text-justify">The Equipment remains at all times the sole and exclusive property of BookMyLens. The borrower has no rights or claims to the Equipment. BookMyLens does not have or make any claim to images or video made by the borrower while using the equipment. All equipment are tested before shipping. It is the borrower’s responsibility to check that the equipment is in good working order once borrower receives the equipment.

                                BookMyLens will not be responsible for any defects or deficiencies in the equipment unless immediate notification has been made. The delivery person will stay for 10 minutes so the customer can check the equipment for minor and major issues and make a note of the same on the delivery slip. Minor issues being scratch on the lens body or lens cap missing etc., In case there is any major issue like IS not working or not able to take images using this lens etc., the lens can be returned to the delivery person.

                                By agreeing to our terms of rent the borrower accepts that should another person sign for the equipment on his/her behalf then that person is authorised by borrower to carry out the above checks. It is the borrower’s responsibility to ensure the equipment on rent is suitable for the purposes for which the borrower requires. We do not allow rehire of our equipment to third parties and no cross hires of any kind are permitted.</p>



                                <p class="text-color"><b> Loss or damage to equipment (Lost/Stolen/Damage)</b></p>



                                <p class="text-justify">The borrower shall be responsible for the safe keeping of the equipment [lens & accessories as applicable] throughout the rent period and shall be liable to BookMyLens for all loss of or damage to the equipment howsoever caused. Borrower shall not materially modify or alter the Equipment. In the event of any material modifications, borrower will be responsible for all reasonable costs of BookMyLens in restoring the Equipment to its normal condition. The borrower shall be required to repackage the equipment as delivered at the time of return to BookMyLens. Please make sure to go through the checklist so that none of the [lens & accessories as applicable] is missing.

                                Borrower shall notify BookMyLens for any loss or damage to the equipment on rent immediately if such loss or damage is sustained.

                                Borrower shall pay to BookMyLens all costs for repairs along with the daily rental charge until the equipment can be repaired or replaced.

                                If the equipment on rent are not returned after the rental tenure in time and if the borrower is not reachable via email/phone, BookMyLens will take it forward with the third party (security agency) for further investigation and recovery of the equipment.</p>



                                <p class="text-color"><b> Cancellation</b></p>



                                <p class="text-justify">If an order is cancelled by the borrower within 4 working days from the date when the period of rent was to commence, the borrower will be liable to pay BookMyLens a cancellation charge equal to and not exceeding the agreed rental charge.</p>



                                <p class="text-color"><b> Reservation</b></p>



                                <p class="text-justify">Upon booking of equipment at least 4 days prior to the rental start date, by telephone or e-mail, BookMyLens shall block the equipment for 24 hrs during which the borrower needs to make an advance payment of 50% of the total rental amount or one day’s rental charge.

                                It is mandatory to provide an acceptable proof of identity through e-mail before the equipment is dispatched.

                                Important Note: In the event of not receiving valid proof of identity and BookMyLens is unable to contact borrower [phone or email], the reservation will be cancelled without any further notice.</p>



                                <p class="text-color"><b> Delivery</b></p>



                                <p class="text-justify">The lens will be sent so that it arrives in time for the beginning of borrower hire period this will normally be the day before Day1.

                                There are two delivery options for customers.<p>

                                <p class="text-justify">The lens will be sent through our authorized executive so that it arrives on the day before Day 1 of the borrower’s rental period.</p>

                                <p class="text-justify">The borrower can come to the BookMylens office and collect the equipment one day prior to the start of the borrower’s rental period. (This is with a prior appointment with the BookMyLens Team).</p>



                                <p class="text-justify">A typical 1 Day rent works like this: Start date of the rental: Jan 10th 2012 </p>

                                <p class="text-justify">End date of the rental: Jan 10th 2012 </p>

                                <p class="text-justify">Equipment Pickup date: Jan 9th 2012 (Evening 5:00 PM to 7:00 PM)</p>

                                <p class="text-justify">Equipment Return date: Jan 11th 2012 (Morning 10:00 AM to 12:00 PM)</p>



                                <p class="text-color"><b> Collection</b></p>



                                <p class="text-justify">The equipment needs to be personally returned to the BookMyLens office. All liability for the equipment remains with the hiring parties until the safe return of the equipment to BookMyLens.</p>



                                <p class="text-color"><b> Payment</b></p>



                                <p class="text-justify">Before any equipment is dispatched or personally collected at the BookMyLens office; 100% payment for the entire rental period must be made.</p>



                                <p class="text-color"><b> Usage of equipment outside INDIA</b></p>

                                <p class="text-justify">You must notify BookMyLens of your intention to take the equipment outside India and gain the permission to do so. You will declare the equipment with the customs prior to leaving India. You are responsible for any custom fee imposed on the equipment, if you fail to declare the equipments with the customs.</p>

                                <div class="clearfix"></div>

                                <br>

                                <?php if($terms==0)

                                { ?>



                                <div id="freezecontent" class="margin-left--10px width-63-5">



                                  <div class="col-sm-6 col-md-6">

                                      <div class="margin-top-5px margin-left-10px color-000 font-family-helvetica font-weight-bold">

                                        <input type="checkbox"  id="terms" value="" checked>Accept the Terms and Conditions.

                                      </div>

                                  </div>

                                  <div class="col-sm-offset-4 col-md-offset-4 col-sm-2 col-md-2">

                                    <a href="myaccount/updateTerms" class="btn btn-danger"> ACCEPT </a>

                                  </div>



                                </div>



                              <?php } ?>



              </div>

          </div>





        </div>





    </div>

  </div>

</div>

<div class="col-sm-2 col-md-2"></div>



<script type="text/javascript">

  $(function(){

    $('.childrow').hide();

    $('.mainrow .btn').on('click', function(){

      id = $(this).parent().parent().data('id');

      if( $('.childrow_'+id).is(':visible'))

      {

        $(this).html('show');

        $('.childrow_'+id).hide();

      }

      else

      {

        $(this).html('hide');

        $('.childrow_'+id).show();

      }

    });



window.alert = (function() {

    var nativeAlert = window.alert;

    return function(message) {

        message.indexOf("DataTables warning") === 0 ?

            console.warn(message) :

            nativeAlert(message);

    }

})();



    var table = $('.table.datatable').DataTable( {

        "bPaginate": true,

        "bLengthChange": false,

        "bFilter": false,

        "bSort": false,

        "bInfo": false,

        "bAutoWidth": false

    } );



  });



  function showPaymentForm($orderID)

  {

    $('#paymentForm').removeClass('hide');

    $('#paymentDiv').addClass('hide');

  }



  function cloasForm()

  {

    $('#paymentDiv').removeClass('hide');

    $('#paymentForm').addClass('hide');

  }



</script>

<style type="text/css">

  .dataTables_paginate.paging_simple_numbers{

    float: right;

  }

  .ui-datepicker-title{

	color:black;

  }

</style>

