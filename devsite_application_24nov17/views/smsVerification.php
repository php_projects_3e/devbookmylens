


<?php echo form_open('smsVerification/checkOptionsSelected',['class'=>'form-horizontal','id'=>'otpVerificationForm']); ?>
<!-- <form class="form-horizontal" action="<?php //echo site_url("smsVerification/checkOptionsSelected");?>" method="post"> -->
<fieldset>
    <legend>Enter Your OTP number</legend>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label"></label>
                <div class="col-lg-10">
                    <!-- <input type="hidden" class="form-control" placeholder="First name" name="email" value="value="<?php //if($this->user_session->isSessionVarExists('email'))  echo $this->user_session->getSessionVar('email');?>"> -->
                    <?php if($this->user_session->isSessionVarExists('email')){
                        $email=$this->user_session->getSessionVar('email');
                        echo form_input(['name'=>'email','type'=>'hidden','class'=>'form-control','placeholder'=>'Email','value'=>$email]);
                        }else{?>
                             
                            <h4><?php echo form_error('email');}?></h4>
                            
                         
                    <!-- <input type="hidden" class="form-control" id="inputEmail" placeholder="Email"> -->
                </div>
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="col-lg-10 col-lg-offset-2">
                    <h4><?php echo form_error('otp');?></h4>
                </div>
                <div class="form-group">
                        
                    <label for="inputPassword" class="col-lg-2 control-label">OTP: </label>
                        <div class="col-lg-10">
                            <!-- <input type="text" class="form-control" placeholder="Enter Your OTP" name="otp"> -->
                            <?php echo form_input(['name'=>'otp','type'=>'text','class'=>'form-control','placeholder'=>'Enter OTP number']);?>
                            <!-- <input type="text" class="form-control" id="inputPassword" placeholder="otp number"> -->
                        </div>
                </div>
            </div>
        </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <!-- <input type="submit" class="btn btn-success" value="VERIFY OTP" name="submit"> -->
                    <?php echo form_submit(['type'=>'submit','value'=>'Verify OTP','name'=>'submit','class'=>'btn btn-success']);?>
                    <!-- <button type="submit" class="btn btn-success">Verify OTP</button> -->
                    <?php echo form_submit(['type'=>'submit','value'=>'Send OTP','name'=>'submit','class'=>'btn btn-primary','formaction'=>site_url('smsVerification/generateOTPNumber')]);?>
                    <!-- <button type="reset" class="btn btn-primary">Generate OTP</button> -->
                </div>
            </div>
</fieldset>
</form>
