          <?php if(!isset($showLapse)) $showLapse = false;?>
		  <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <table id="orderList" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th><th></th>
                        <th>Order Number</th>
                        <th>Client Name</th>
						<th>Contact Number</th>
                        <th>Total Amount</th>
						<th>Collect Date</th>
						<th>Start Date</th>
						<th>End Date</th>
                        <th>Return Date</th>
						<th>Products</th>
						<?php if($showLapse) {?>
						<th>Payment Pending</th>
						<th>Payment Lapse</th>
						<?php } ?>
                        <th></th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th></th><th></th>
                        <th>Order Number</th>
                        <th>Client Name</th>
						<th>Contact Number</th>
                        <th>Total Amount</th>
						<th>Collect Date</th>
						<th>Start Date</th>
						<th>End Date</th>
                        <th>Return Date</th>
						<th>Products</th>
						<?php if($showLapse) {?>
						<th>Payment Pending</th>
						<th>Payment Lapse</th>
						<?php } ?>
						<th></th>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php foreach ($tableRows as $row) { ?>
                        <tr>
                          <?php
                            foreach ($row as $colName => $col) {
								if($colName == "paid" || $colName == "total"){}
								else if($colName == "startDate" || $colName == "endDate" || $colName == "collectDate" || $colName == "returnDate")
									echo "<td>".dateFromMysqlDate($col)."</td>";
								else
									echo "<td>$col</td>";
                            }
							$lapsed = "";
							if($row['paid'] < $row['total']){
								$dStart = new DateTime($row['endDate']);
								$dEnd  = new DateTime();
								$dDiff = $dStart->diff($dEnd);
								$lapsed = $dDiff->days;
							}
							?>
							
							<?php if($showLapse) {?>
							<td><?php echo ($row['total'] - $row['paid']); ?></td>
							<td><?php echo ($lapsed <0)?"":(($lapsed == "")?"paid":$lapsed); ?></td>
							<?php } ?>
						  <td><a target ='_blank' href ='<?php echo admin_url('orders/editOrder/'.$row['subOrderID']);?>' class='btn-transparent fa fa-pencil-square-o text-warning'>Edit</a></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div><!-- /.content-wrapper -->
<script type="text/javascript">

nxtStatus = "<?php echo $nextStatus; ?>";
$(document).ready(function() {

    $('#orderList tfoot th').each( function () {
        var title = $('#orderList thead th').eq( $(this).index() ).text();
        if(title != '')
          $(this).html( '<input class="width-100" type="text" placeholder="Search '+title+'" />' );
    } );

    var table = $('#orderList').DataTable( {
        dom: 'T<"clear">lrtip',
        tableTools: {
            "sSwfPath": "<?php echo base_url(); ?>assets/dist/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            aButtons : ["select_all", "select_none",
                          {"sExtends":"csv", "mColumns": "visible"},
                          {"sExtends":"xls", "mColumns": "visible"},
                          {"sExtends":"pdf", "mColumns": "visible"},
                        {
                          "sExtends": "ajax", "mColumns": [0],
                          "sButtonText" : "<?php echo $nextStatus;?>",
                          "fnAjaxComplete":function(){
                            alert("success");
                            window.location.reload(true);
                          },
                          "bSelectedOnly" : true,"sButtonClass":"action-button hide",
                          "sAjaxUrl": "<?php echo admin_url('/orders/bulkAction/').$nextStatus;?>",
                          "fnSelect": function ( nButton, oConfig, nRow ) {
                              if(TableTools.fnGetInstance('orderList').fnGetSelectedIndexes().length > 0 && nxtStatus != '')
                              {
                                $('.action-button').removeClass('hide');
                              }
                              else
                              {
                                $('.action-button').addClass('hide');
                              }

                          },
                          "fnClick": function( nButton, oConfig )
                            {
                              var sData = this.fnGetTableData(oConfig);
                              $.ajax(
                              {
                                "url": oConfig.sAjaxUrl,
                                "data": [
                                    { "name": "tableData", "value": sData , },
                                ],
                                "success": oConfig.fnAjaxComplete,
                                "type": "POST",
                                "cache": false,
                                "error": function ()
                                    {
                                    alert( "Error detected when sending table data to server" );
                                    }
                              });
                            }
                        }
                      ],
            "sRowSelect": "multi"
        },
        "bPaginate": true,
		"bLengthChange": true,
		"iDisplayLength": 50,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        columnDefs: [
            {
                "targets": [ 0,1 ],
                "visible": false,
                "searchable": false
            },
        ],
        "order": [[7, 'asc']]
    } );

    // Apply the search
    table.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

    statusTextArr = {'booked':'Approve','approved':'Rent Out','on rent':'Returned','returned':'Compelete','reserved':'approved'};
    statusUpdateArr = {'booked':'APPROVE','approved':'RENT','on rent':'RETURNED','returned':'COMPLETE', 'reserved':'APPROVE'};
/*
    // Add event listener for opening and closing details
    $('#orderList tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        d = row.data();
        status = d[10];
        $.getJSON("<?php echo admin_url('orders/orderDetails/');?>"+d[0]+'/'+d[1], function(result){
          //console.log(result);
          data = "<table class='table'>"+
                    "<thead><tr>"+
                      "<th>Name</th>"+
                      "<th>Collect Date</th>"+
                      "<th>Start Date</th>"+
                      "<th>End Date</th>"+
                      "<th>Reurn Date</th>"+
                      "<th>Status</th>"+
                    "</tr></thead>";
          $.each(result.itemList, function(i, object){
              data += "<tr>";
              data += "<td>"+object.itemName+"</td>";
              data += "<td>"+$.datepicker.formatDate('dd M yy', new Date(object.collectDate))+"</td>";
              data += "<td>"+$.datepicker.formatDate('dd M yy', new Date(object.startDate))+"</td>";
              data += "<td>"+$.datepicker.formatDate('dd M yy', new Date(object.endDate))+"</td>";
              data += "<td>"+$.datepicker.formatDate('dd M yy', new Date(object.returnDate))+"</td>";
              data += "<td>"+object.orderStatusDisplayName+"</td>";
              data += "</tr>";
          });
          data += "</table>";
          pickupAddr = result.address['pickup'];
          returnAddr = result.address['delievery'];
          data += "<div class='row'><div class='col-md-10'>";
          data += "<div class= 'row'><div class='col-md-6'><b>PickUp Address : </b>";
          if(pickupAddr != undefined) {
            data += pickupAddr.addressLine1+","+pickupAddr.addressLine2+","+pickupAddr.addressCity+","+pickupAddr.addressState+","+pickupAddr.addressPin+"</div><div class='col-md-2'> <b>Landmark : </b>"+pickupAddr.addressLandmark+" </div><div class='col-md-2'><b>Shipping Cost :</b>"+pickupAddr.shippingCost+"</div><div class='col-md-2'><b>Address Type :</b>"+pickupAddr.addressType;
          }
          data += "</div></div>";

          data += "<div class= 'row'><div class='col-md-6'><b>Return Address : </b>";
          if(returnAddr != undefined) {
            data += returnAddr.addressLine1+","+returnAddr.addressLine2+","+returnAddr.addressCity+","+returnAddr.addressState+","+returnAddr.addressPin+"</div><div class='col-md-2'> <b>Landmark : </b>"+returnAddr.addressLandmark+" </div><div class='col-md-2'><b>Shipping Cost :</b>"+returnAddr.shippingCost+"</div><div class='col-md-2'><b>Address Type :</b>"+returnAddr.addressType;
          }
          data += "</div></div>";

          data += "</div>";
          data += "<div class='col-md-2'>"
                    +"<div class='col-md-12'>"
                      +"<a target ='_blank' href ='<?php echo admin_url('orders/editOrder/');?>"+d[0]+"' class='btn-transparent fa fa-pencil-square-o text-warning' title='EDIT'>"
                        +"Edit"
                      +"</a>"
                    +"</div>";
          if(statusTextArr[status] != undefined){
           data+=   "<div class='col-md-12'>"
                      +"<a href ='<?php echo admin_url('orders/updateStatusByCode/');?>"+d[0]+"/"+statusUpdateArr[status]+"' class='btn-transparent fa fa-check text-success'>"
                        +statusTextArr[status]+
                      "</a>"
                    +"</div>";
          }
           data+=  "</div>";
          data += "</div>";

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( data ).show();
              tr.addClass('shown');
          }
        });

    } );
*/
	<?php if($status != "COMPLETE" || $status != "CANCEL"){ ?>
	//$('td.details-control').click();
	<?php } ?>
} );
</script>

<style type="text/css">
  tbody td.details-control {
    background: url("<?php echo site_url('images/details_open.png');?>") no-repeat center center;
    cursor: pointer;
  }
  tbody tr.details td.details-control {
      background: url("<?php echo site_url('images/details_close.png');?>") no-repeat center center;
  }

</style>
